const db = require('../models');
const episodeMetrics = require('../models/episode-metrics');
const Sanic = require('sanic.js').Library;

module.exports.test = test;
module.exports.search = search;
module.exports.query = query;
module.exports.annotations = annotations;

let targets = [];
targets.push('Total episodes');
targets.push('Total episodes By Type');
targets.push('Live');
targets.push('VoD');
targets.push('Episodes Filtered By ID')

/**
 * Get session by ID
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function test(req, res) {
    res.status(200).json('OK');
}

/**
 * Get all session records
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function search(req, res) {
    res.status(200).json(targets);
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function annotations(req, res) {
    res.status(200).json({
        timeseries: 'OK'
    });
}

/**
 * Perform query to DB and return datasource object
 * 
 * @param {*} req Request
 * @param {*} res Response
 * @param {*} next Next
 */
async function query(req, res) {
    req = roundTimeRangeForBetterCacheUtilization(req);

    const activeConnection = await db.connect();
    let promises = req.body.targets.map(target => {
        if (target.target === targets[0]) return getTotalEpisodes(req);
        if (target.target === targets[1]) return getTotalEpisodesByType(req);
        if (target.target === targets[2]) return getTotalLiveEpisodes(req);
        if (target.target === targets[3]) return getTotalVoDEpisodes(req);
        if (target.target === targets[4]) return getEpisodesById(req);
    });
    let data = await Promise.all(promises);
    await activeConnection.release();

    res.status(200).json([].concat(...data));
}

/**
 * Get total episodes in single query, live type filtering done by javascript
 * 
 * @param {object} req request
 */
async function getTotalEpisodes(req) {
    let results = await episodeMetrics.getTotalEpisodes(req.body);

    let live = [], vod = [];
    Sanic.Array.map(results, _ => { 
        if(_.live == 1) {
            live.push([_.value, _.time * 1000]);
            return;
        }
        vod.push([_.value, _.time * 1000]);
    });

    return [{
            target: "Live",
            datapoints: live
        },
        {
            target: "VoD",
            datapoints: vod
        }
    ];
}

/**
 * Get total episodes by performing separate query for live and VoD episodes
 * (FOR TESTING AND BENCHMARKING) - performs worse than getTotalEpisodes() function
 * 
 * @param {object} req Request
 */
async function getTotalEpisodesByType(req) {
    let [live, vod] = await Promise.all([
        episodeMetrics.getTotalEpisodesByType(req.body, 1),
        episodeMetrics.getTotalEpisodesByType(req.body, 0)
    ]);

    return [{
            target: "Live",
            datapoints: Sanic.Array.map(live, _ => [_.value, _.time * 1000])
        },
        {
            target: "VoD",
            datapoints: Sanic.Array.map(vod, _ => [_.value, _.time * 1000])
        }
    ];
}

/**
 * Get Live episodes
 * 
 * @param {object} req Request
 */
async function getTotalLiveEpisodes(req) {
    let live = await episodeMetrics.getTotalEpisodesByType(req.body, 1);
    live = Sanic.Array.map(live, _ => [_.value, _.time * 1000]);

    return [{
        target: "Live",
        datapoints: live
    }];
}

/**
 * Get Video on demand episodes
 * 
 * @param {object} req Request
 */
async function getTotalVoDEpisodes(req) {
    let vod = await episodeMetrics.getTotalEpisodesByType(req.body, 0);
    vod = Sanic.Array.map(vod, _ => [_.value, _.time * 1000]);

    return [{
        target: "VoD",
        datapoints: vod
    }];
}

async function getEpisodesById(req) {
    let [episodeContentIds, episodeMetricsRows] = await episodeMetrics.getEpisodesById(req.body);

    let ecids_lookup_table = [];
    Sanic.Array.map(episodeContentIds, _ => ecids_lookup_table[_.episode_id] = _.episode_content_id);
    let episodesWithContentId = Sanic.Array.map(episodeMetricsRows, episodeMetric => {        
        episodeMetric.episode_id = ecids_lookup_table[episodeMetric.episode_id];
        return episodeMetric;
    });

    return Sanic.Array.map(req.body.scopedVars.episode_id.value, targetEpisodeId => {
        let datapoints = Sanic.Array.reduce(
            Sanic.Array.filter(episodesWithContentId, x => x.episode_id == targetEpisodeId),
            (p, c) => {
                var time = c.time * 1000;
                if (!p.hasOwnProperty(time)) {
                    p[time] = c.value;
                    return p;
                }
                p[time] = p[time] + c.value;
                return p;
            }, {});

        return {
            target: targetEpisodeId,
            datapoints: Sanic.Array.map(Object.keys(datapoints), key => [datapoints[key], key])
        };
    });
}

const pruneOldData = require('../../crons/prune-old-data');
const pruneOldDataInBatches = require('../../crons/prune-old-data-in-batches');
const optimizeMysqlTables = require('../../crons/optimize-mysql-tables');
const precalculateEpisodeMetrics = require('../../crons/precalculate-episode-metrics');

module.exports.testPruneOldDataCron = testPruneOldDataCron;
module.exports.testPruneDbInBatchesCron = testPruneDbInBatchesCron;
module.exports.testOptimizeMysqlTablesCron = testOptimizeMysqlTablesCron;
module.exports.testPrecalculateEpisodeMetricsCron = testPrecalculateEpisodeMetricsCron;

/**
 * Testing Cron 
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function testPruneOldDataCron(req, res) {
    await pruneOldData();
    
    res.status(200).json('OK');
}

/**
 * Testing Cron 
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function testPruneDbInBatchesCron(req, res) {
    await pruneOldDataInBatches();
    console.log('SUCCESSFULL PRUNE OF OLD DATABASE DATA');

    res.status(200).json('OK');
}

/**
 * Testing Cron 
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function testOptimizeMysqlTablesCron(req, res) {
    await optimizeMysqlTables();
    console.log('SUCCESSFULL OPTIMIZING MYSQL TABLES');

    res.status(200).json('OK');
}

/**
 * Testing Cron 
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function testPrecalculateEpisodeMetricsCron(req, res) {
    await precalculateEpisodeMetrics();
    console.log('SUCCESSFULL PRECALCULATING EPISODE METRICS');

    res.status(200).json('OK');
}
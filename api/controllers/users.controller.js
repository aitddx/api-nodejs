const db = require('../models');
const jwt = require('jsonwebtoken');
const users = require('../models/users');
const sessions = require('../models/sessions');

module.exports.post = post;
module.exports.get = get;
module.exports.getAll = getAll;

/**
 * Get user by ID
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function get(req, res, next) {
    const id = req.params.id;

    const activeConnection = await db.connect();
    let res_users = await users.find({ id });
    let res_sessions = await sessions.get();
    await activeConnection.release();

    res.status(200).json({
        data: {
            users: res_users,
            sessions: res_sessions
        }
    });
}

/**
 * Get user by ID
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function getAll(req, res, next) {

    const activeConnection = await db.connect();
    let results = await users.find({id: req.userId});
    await activeConnection.release();

    res.status(200).json({
        data: results
    });
}

/**
 * Get user by ID
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function post(req, res, next) {
    const uuid = req.body.uuid;

    const activeConnection = await db.connect();
    let results = await users.findOrCreate({ uuid });
    await activeConnection.release();

    let token = jwt.sign(
        { userId: results },
        process.env.APP_SECRET,
        { expiresIn: "72h" }
    );

    res.status(200).json({
        tester: true,
        token,
        message: "Auth successful",
    });
}
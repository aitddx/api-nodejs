const db = require('../models');
const episodeMetrics = require('../models/episode-metrics');
const sessions = require('../models/sessions');
const client_versions = require('../models/client-versions');
const { logger } = require('../middlewares/logger');
const redis = require('../services/redis');

const profiler = require('../services/profiler');

module.exports.dbStatus = dbStatus;
module.exports.dbStatusPoolQuery = dbStatusPoolQuery;
module.exports.maxThroughput = maxThroughput;
module.exports.testPlatformHeaders = testPlatformHeaders;

/**
 * Testing method for api performance benchmarking for DB connections
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function dbStatus(req, res) {
    // profiler.start('db-status');

    const activeConnection = await db.connect();
    // await activeConnection.beginTransaction();
    const data = await testDbHelper();
    // await activeConnection.rollback();
    await activeConnection.release();

    res.status(200).json(data);
    // profiler.stop();
}

/**
 * Testing method for api performance benchmarking for DB connections
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function dbStatusPoolQuery(req, res) {
    db.connect = shadowConnect;
    db.query = poolQuery;

    await db.connect();
    const data = await testDbHelper();
    res.status(200).json(data);
}

async function maxThroughput(req, res) {
    res.json('OK');
}

/**
 * Get session by ID
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function testPlatformHeaders(req, res) {
    const platform = req.headers['x-qs-platform'] || 'unknown';

    res.status(200).json({
        data: platform
    });
}

async function testDbHelper() {
    let data = await episodeMetrics.callTestDb();
    await sessions.isAlreadyRecorded(2, '1533882013');
    await client_versions.findOrCreate({
        version: '3.2.1',
        platform_id: null
    });

    data = data[0] ? {} : null;
    if (!data) {
        const error = new Error('No records found')
        error.status = 422;
        throw error;
    };

    data["Time"] = (new Date()).getTime();
    data["DatabaseStatus"] = "Up";

    return data;
}

function shadowConnect() {
    logger.info('using shadow connect')
    this.redisClient = this.redisClient || Object.create(redis).init();
}

function poolQuery(query, params) {
    logger.info('using shadow query')
    return this.pool.query(query, params);
}
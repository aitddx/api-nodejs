const db = require('../models');
const episodeMetrics = require('../models/episode-metrics');
const Sanic = require('sanic.js').Library;

const {
    getAvgPerISP,
    getAvgPerOS,
    getAvgPerInterface,
    roundTimeRangeForBetterCacheUtilization
} = require('../helpers/datasources');

module.exports.test = test;
module.exports.search = search;
module.exports.query = query;
module.exports.annotations = annotations;

let targets = [];
targets.push('Total episodes');
targets.push('LIVE: Average stalling number per ISP');
targets.push('VoD: Average stalling number per ISP');
targets.push('LIVE: Avg. Stalling number per OS version');
targets.push('VoD: Avg. Stalling number per OS version');
targets.push('LIVE: Avg. Stalling number per Network Interface')
targets.push('VoD: Avg. Stalling number per Network Interface')

/**
 * Get session by ID
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function test(req, res) {
    res.status(200).json('OK');
}

/**
 * Get all session records
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function search(req, res) {
    res.status(200).json(targets);
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function annotations(req, res) {
    res.status(200).json({
        timeseries: 'OK'
    });
}

/**
 * Perform query to DB and return datasource object
 * 
 * @param {*} req Request
 * @param {*} res Response
 * @param {*} next Next
 */
async function query(req, res) {
    req = roundTimeRangeForBetterCacheUtilization(req);

    const activeConnection = await db.connect();
    let promises = req.body.targets.map(target => {
        if (target.target === targets[0]) return getTotalEpisodes(req);
        if (target.target === targets[1]) return getAvgStallingPerISP(req);
        if (target.target === targets[2]) return getAvgStallingPerISP(req, 0);
        if (target.target === targets[3]) return getAvgStallingPerOS(req);
        if (target.target === targets[4]) return getAvgStallingPerOS(req, 0);
        if (target.target === targets[5]) return getAvgStallingPerInterface(req);
        if (target.target === targets[6]) return getAvgStallingPerInterface(req, 0);
    });
    let data = await Promise.all(promises).catch(console.error);
    await activeConnection.release();

    res.status(200).json([].concat(...data));
}

/**
 * Get total episodes in single query, live type filtering done by javascript
 * 
 * @param {object} req request
 */
async function getTotalEpisodes(req) {
    let results = await episodeMetrics.getTotalEpisodes(req.body);

    let live = [],
        vod = [];
    Sanic.Array.map(results, _ => {
        if (_.live == 1) {
            live.push([_.value, _.time * 1000]);
            return;
        }
        vod.push([_.value, _.time * 1000]);
    });

    return [{
            target: "Live",
            datapoints: live
        },
        {
            target: "VoD",
            datapoints: vod
        }
    ];
}

/**
 * Get Avg Initial delay per ISP
 * 
 * @param {object} req Request
 */
async function getAvgStallingPerISP(req, live = 1) {
    return await getAvgPerISP(req, episodeMetrics.getRawStallingNumber, live);
}

/**
 * Get Avg Initial delay per OS version
 * 
 * @param {object} req Request
 */
async function getAvgStallingPerOS(req, live = 1) {
    return await getAvgPerOS(req, episodeMetrics.getRawStallingNumber, live);
}

/**
 * Get Avg Initial delay per Interface
 * 
 * @param {object} req Request
 */
async function getAvgStallingPerInterface(req, live = 1) {
    return await getAvgPerInterface(req, episodeMetrics.getRawStallingNumber, live);
}
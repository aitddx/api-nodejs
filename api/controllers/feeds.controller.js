const request = require("request");
const db = require('../models');
const users = require('../models/users');
const sessions = require('../models/sessions');

module.exports.get = get;

/**
 * Get user by ID
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function get(req, res) {
    const skip = req.params.skip || 0;

    request.get(`https://62.218.45.46/api/v1/results/orig/${skip}`, {rejectUnauthorized: false }, async (err, response, data) => {
        if (err) { throw new Error(err) };
        console.log('feeders');

        const activeConnection = await db.connect();
        // @TODO replace with real implementation of user_id from JWT!
        const user_id = await users.findOrCreate({ uuid: 'feeder' });
        for (const session of JSON.parse(data)) {
            let recorded = await sessions.isAlreadyRecorded(user_id, session.session_start);
            if (!recorded){ await sessions.store(user_id, session); }
        }
        await activeConnection.release();

        res.status(200).json({
            data: "OK"
        });
    });
}
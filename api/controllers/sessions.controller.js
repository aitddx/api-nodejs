const db = require('../models');
const sessions = require('../models/sessions');
const sessionsMongoCollection = require("../models/sessions.mongo");

module.exports.create = create;
module.exports.createV2 = createV2;
module.exports.createFromWeb = createFromWeb;
module.exports.get = get;
module.exports.getAll = getAll;

/**
 * Get session by ID
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function get(req, res) {
    const id = req.params.id;
    res.status(200).json({
        data: id
    });
}

/**
 * Get all session records
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function getAll(req, res) {
    res.status(200).json({
        data: 'all sessions'
    })
}

/**
 * Create new session
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function create(req, res) {
    if (!req.body.data) sendMissingInputError();

    const userId = req.userId
    // Data might come in as urlencoded string
    const data = urlDecodeIfNotJson(req.body.data);
    // Assuming that everything that comes to v1 API EP always comes from iOS
    data.session['platform'] = 'ios';

    const activeConnection = await db.connect();
    let recorded = await sessions.isAlreadyRecorded(userId, data.session.session_start, data.session.platform);
    if (recorded) {
        await activeConnection.release();
        res.status(200).json({
            data: 'Already recorded',
            status: 200
        });
    } else {
        await sessionsMongoCollection.store(userId, data.session);
        await sessions.store(userId, data.session);
        await activeConnection.release();

        res.status(201).json({
            data: 'OK',
            status: 200
        });
    }
}

/**
 * Create new session
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function createV2(req, res, next) {
    if (!req.body.data) sendMissingInputError();

    const userId = req.userId
    // Data might come in as urlencoded string
    let data = urlDecodeIfNotJson(req.body.data);
    data = checkPlatform(req, data);

    const activeConnection = await db.connect();
    let recorded = await sessions.isAlreadyRecorded(userId, data.session.session_start, data.session.platform);
    // recorded = false;
    if (recorded) {
        await activeConnection.release();
        res.status(200).json({
            data: 'Already recorded',
            status: 200
        });
    } else {
        await sessionsMongoCollection.store(userId, data.session, false);
        let result = await sessions.store(userId, data.session);
        await activeConnection.release();
        res.status(201).json({
            data: result,
            status: 200
        });
    }
}

/**
 * Create new session from web player session data
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function createFromWeb(req, res, next) {
    if (!req.body.data) sendMissingInputError();

    const userId = req.userId
    // Data might come in as urlencoded string
    let data = urlDecodeIfNotJson(req.body.data);
    // Possible to directly set platform to web
    data = checkPlatform(req, data);

    const activeConnection = await db.connect();
    let recorded = await sessions.isAlreadyRecorded(userId, data.session.session_start, data.session.platform);
    // recorded = false;
    if (recorded) {
        await activeConnection.release();
        res.status(200).json({
            data: 'Already recorded',
            status: 200
        });
    } else {
        await sessionsMongoCollection.store(userId, data.session, false);
        let result = await sessions.store(userId, data.session);
        await activeConnection.release();
        res.status(201).json({
            data: result,
            status: 200
        });
    }
}

/**
 * Decode session string which comes url-encoded
 * 
 * @param {string} data 
 */
function urlDecodeIfNotJson(data) {
    if (typeof data !== 'object') {
        return JSON.parse(decodeURIComponent(data));
    }
    return data;
}

/**
 * Send error response
 * 
 * @param {*} next 
 */
function sendMissingInputError() {
    const error = new Error('Bad Input. Missing \'data\' field');
    error.status = 422;
    throw error;
}

/**
 * Check if platform header is present in the 
 * request and set it to session JSON, for later processing
 * 
 * @param {object} req Original request object
 * @param {object} data Session JSON data
 */
function checkPlatform(req, data) {
    const platform = req.headers['x-qs-platform'] || 'unknown';
    data.session['platform'] = platform;

    return data;
}
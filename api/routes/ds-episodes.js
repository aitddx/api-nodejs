const express = require('express');
const router = express.Router();
const catchErrors = require('../middlewares/catch-errors');
const dsEpisodes = require('../controllers/ds-episodes.controller');

module.exports = router;

router
    .all('/', catchErrors(dsEpisodes.test))
    .post('/search', catchErrors(dsEpisodes.search))
    .post('/query', catchErrors(dsEpisodes.query))
    .post('/annotations', catchErrors(dsEpisodes.annotations));

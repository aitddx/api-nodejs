const express = require('express');
const router = express.Router();
const cronTests = require('../controllers/cron-tests.controller.js');
const catchErrors = require('../middlewares/catch-errors');

module.exports = router;

router
    .get('/prune-db-in-batches', catchErrors(cronTests.testPruneDbInBatchesCron))
    .get('/prune-db', catchErrors(cronTests.testPruneOldDataCron))
    .get('/optimize-db-tables', catchErrors(cronTests.testOptimizeMysqlTablesCron))
    .get('/precalculate-results', catchErrors(cronTests.testPrecalculateEpisodeMetricsCron));
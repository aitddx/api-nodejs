const express = require('express');
const router = express.Router();
const catchErrors = require('../middlewares/catch-errors');
const feedsController = require('../controllers/feeds.controller.js');

module.exports = router;

router.get('/:skip', catchErrors(feedsController.get));

const express = require('express');
const router = express.Router();
const catchErrors = require('../middlewares/catch-errors');
const dsAvgInitialDelay = require('../controllers/ds-avg-initial-delay.controller');

module.exports = router;

router
    .all('/', catchErrors(dsAvgInitialDelay.test))
    .post('/search', catchErrors(dsAvgInitialDelay.search))
    .post('/query', catchErrors(dsAvgInitialDelay.query))
    .post('/annotations', catchErrors(dsAvgInitialDelay.annotations));

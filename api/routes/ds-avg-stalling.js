const express = require('express');
const router = express.Router();
const catchErrors = require('../middlewares/catch-errors');
const dsAvgStalling = require('../controllers/ds-avg-stalling.controller');

module.exports = router;

router
    .all('/', catchErrors(dsAvgStalling.test))
    .post('/search', catchErrors(dsAvgStalling.search))
    .post('/query', catchErrors(dsAvgStalling.query))
    .post('/annotations', catchErrors(dsAvgStalling.annotations));

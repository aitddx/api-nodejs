const express = require('express');
const router = express.Router();
const catchErrors = require('../middlewares/catch-errors');
const sessionsController = require('../controllers/sessions.controller.js');

module.exports = router;

router
    .get('/', catchErrors(sessionsController.getAll))
    .post('/', catchErrors(sessionsController.createV2))
    .post('/web', catchErrors(sessionsController.createFromWeb));

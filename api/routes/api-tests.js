const express = require('express');
const router = express.Router();
const apiTestsController = require('../controllers/api-tests.controller.js');
const catchErrors = require('../middlewares/catch-errors');

module.exports = router;

router
    .get('/db-status', catchErrors(apiTestsController.dbStatus))
    .get('/db-status-pool-query', catchErrors(apiTestsController.dbStatusPoolQuery))
    .get('/max-throughput', catchErrors(apiTestsController.maxThroughput))
    .get('/platformHeaders', catchErrors(apiTestsController.testPlatformHeaders));
const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/auth');
const catchErrors = require('../middlewares/catch-errors');
const usersController = require('../controllers/users.controller.js');

module.exports = router;

router
    .get('/', authMiddleware, catchErrors(usersController.getAll))
    .get('/:id', authMiddleware, catchErrors(usersController.get))
    .post('/', catchErrors(usersController.post));

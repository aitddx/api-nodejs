const db = require('./index');

var episode_contents = Object.assign(Object.create(db), {});
episode_contents.init('episode_contents');

module.exports = episode_contents;

const db = require('./index');

var device_types = Object.assign(Object.create(db), {});
device_types.init('device_types');

module.exports = device_types;

const db = require('./index');
const mysql = require('mysql');
const md5 = require('yamd5.js');

var isps = Object.assign(Object.create(db), {
    getTopIsps
});
isps.init('isps');

module.exports = isps;

async function getTopIsps(from, to, count = 10) {
    let query = `SELECT s.isp_id, i.\`name\` as \`name\`
        FROM sessions as s
        JOIN isps as i
        ON s.isp_id = i.id
        WHERE s.session_start BETWEEN ? AND ?
        GROUP BY s.isp_id
        ORDER BY count(s.isp_id) DESC
        LIMIT ?;`.replace(/(\r\n|\n|\r)/gm, '').replace(/\s+/g, ' ');
    query = mysql.format(query, [from, to, count]);

    return await this.checkDatasourceCacheOrReturnQuery(query);
}
const mongo = require('../services/mongo');

module.exports = {
    store
};

let mongoDb
mongo.connect()
    .then(db => mongoDb = db)
    .catch(err => {
        console.log('Mongo connection error:', err.message);
        process.exit(1);
    });

/**
 * 
 * @param {*} user_id User_id from mysql DB
 * @param {*} session 
 */
async function store(user_id, session, checkDuplicates = true) {
    if (!mongoDb) return;

    let mongoSessions = await mongoDb
        .db('qoestream')
        .collection('sessions');

    if (checkDuplicates) {
        let mongoExists = await mongoSessions.findOne({
            user_id: user_id,
            session_start: session.session_start
        });

        if (mongoExists) return {
            insertedId: 'Already recorded'
        }
    }

    let res = await mongoSessions.insertOne({
        user_id: user_id,
        ...session
    });
    // await mongoDb.close();

    return res;
}
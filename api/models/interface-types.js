const db = require('./index');
const mysql = require('mysql');
const md5 = require('yamd5.js');

var interface_types = Object.assign(Object.create(db), {
    getTopInterfaces
});
interface_types.init('interface_types');

async function getTopInterfaces(from, to) {
    let query = `SELECT s.interface_type_id, i.\`type\` as \`name\`
        FROM sessions as s
        JOIN interface_types as i
        ON s.interface_type_id = i.id
        WHERE s.session_start BETWEEN ? AND ?
        GROUP BY s.interface_type_id
        ORDER BY count(s.interface_type_id) DESC
        LIMIT 5;`.replace(/(\r\n|\n|\r)/gm, '').replace(/\s+/g, ' ');
    query = mysql.format(query, [from, to]);

    return await this.checkDatasourceCacheOrReturnQuery(query);
}

module.exports = interface_types;

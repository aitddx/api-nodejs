const db = require('./index');
const mysql = require('mysql');

var platforms = Object.assign(Object.create(db), {
    findOrCreate
});
platforms.init('platforms');

module.exports = platforms;

/**
 * @TODO Change with DB lookup, 
 * but test performance impact of storing it in DB first!
 * 
 * Shadow implementation of default findOrCreate, 
 * to limit calls to DB by hardcoding platform codes into here
 * 
 * @param {Object} obj Findable properties mappings
 */
function findOrCreate(obj) {
    const platform = obj.name;
    let code;

    switch (String(platform).toLowerCase()) {
        case 'ios':
            code = 1;
            break;
        case 'android':
            code = 2;
            break;
        case 'web':
            code = 3;
            break;
        default:
            code = 0;
            break;
    }

    return code;
}
const db = require('./index');

var client_versions = Object.assign(Object.create(db), {});
client_versions.init('client_versions');

module.exports = client_versions;
const db = require('./index');
const mysql = require('mysql');

const readable = ['uuid', 'tester'];
const writable = [...readable];

var users = Object.assign(Object.create(db), { 
    create,
    get
});
users.init('users', readable, writable );
module.exports = users;

function create(userObj, tester = false) {
    return this.query(mysql.format(
        'INSERT INTO ?? (??) VALUES ?', 
        [this.tableName, this.writableAttributes, [[userObj.uuid, tester]]]
    ));
}

async function get() {
    return this.query(mysql.format(
        'SELECT * FROM ??', [this.tableName]));
}
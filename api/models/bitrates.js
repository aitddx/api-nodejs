const db = require('./index');
const mysql = require('mysql');

let writable = ['episode_id', 'bitrate', 'timestamp'];
let readble = ['episode_id', 'bitrate', 'timestamp'];

var bitrates = Object.assign(Object.create(db), {
    create,
    createFromPrepared,
    prepareForSave
});
bitrates.init('bitrates', readble, writable);

module.exports = bitrates;

async function create(episode_id, bitrates) {
    if (!bitrates) return;
    bitrates = this.prepareForSave(episode_id, bitrates);

    return await this.query(mysql.format(
        'INSERT INTO ?? (??) VALUES ?',
        [this.tableName, this.writableAttributes, bitrates]
    ));
}

function createFromPrepared(bitrates) {
    if (bitrates.length === 0) return;

    return this.query(mysql.format(
        'INSERT INTO ?? (??) VALUES ?', 
        [this.tableName, this.writableAttributes, bitrates]
    ));
}

function prepareForSave(episode_id, bitrates) {
    if (!bitrates) return;
    
    return bitrates.map(bitrate => [
        episode_id, 
        Math.round(bitrate.bitrate / 1024), 
        this.convertToMysqlTimestamp(bitrate.timestamp)
    ]);
}

const mysql = require('promise-mysql');
const { logger } = require('../middlewares/logger');
const redis = require('../services/redis');
const md5 = require('yamd5.js');

const pool = mysql.createPool({
    host: process.env.MYSQL_HOST || 'database',
    user: process.env.MYSQL_USER || 'root',
    password: process.env.MYSQL_ROOT_PASSWORD || 'root',
    database: process.env.MYSQL_DATABASE || 'qoestream',
    multipleStatements: true,
    connectionLimit: 130,
    waitForConnections: true,
    debug: false
});

if (process.env.APP_ENV === 'development') {
    pool.on('connection', connection => {
        logger.info('Connection %d opened', connection.threadId);
        connection.on('enqueue', sequence => {
            if ('Query' === sequence.constructor.name) {
                logger.info(sequence.sql);
            }
        });
    });

    pool.on('acquire', function (connection) {
        logger.info('Connection %d acquired',connection.threadId);
    });

    pool.on('release', connection => {
        logger.info('Connection %d released', connection.threadId);
    });
}

var database = {
    pool: pool,
    redisClient: undefined,

    init(tableName, readableAttributes = ['id'], writableAttributes = ['id']) {
        this.tableName = tableName;
        this.readableAttributes = readableAttributes;
        this.writableAttributes = writableAttributes;
    },

    async connect() {
        this.connection = await this.pool.getConnection();
        this.redisClient = this.redisClient || Object.create(redis).init();

        return this.connection;
    },

    /**
     * Find in DB table by property:value pairs
     * 
     * @param {property: value} obj property:value pair for finding in Table
     * @param {number} limit Required number of rows returned
     */
    async find(obj, limit = 0) {
        if (!obj || !Object.values(obj)[0]) {
            throw new Error('Missing parameters for find query');
        }
        
        const redisKey = this.tableName + '_' + md5.YaMD5.hashStr(Object.values(obj).toString() + limit);

        let cached = await this.redisClient.getValue(redisKey);
        if (cached) return JSON.parse(cached);

        let nonCached = await this.query(this.prepareSelectQuery(obj, limit));
        if (nonCached.length > 0) {
            await this.redisClient.setValue(redisKey, JSON.stringify(nonCached));
        }

        return nonCached;
    },

    /**
     * Find or create new record in DB
     * 
     * @param {property: value} obj Property: Table columns name, value: search value
     */
    async findOrCreate(obj) {
        var rows = await this.find(obj);
        var id = rows[0] ? rows[0].id : null;
        if (!id) {
            rows = await this.create(obj);
            id = rows.insertId;
        };

        return id;
    },

    /**
     * Create new DB row with given column:value pairs
     * 
     * @param {Object} obj column:value pairs in JS Object
     */
    create(obj) {
        return this.query(mysql.format(
            'INSERT INTO ?? (??) VALUES (?)',
            [this.tableName, Object.keys(obj), Object.values(obj)]
        ));
    },
    
    /**
     * Convert unix ms timestamp to MySQL TIMESTAMP(3) format
     * 
     * @param {int} unixTimestamp Unix timestamp in miliseconds
     */
    convertToMysqlTimestamp(unixTimestamp) {
        return new Date(+unixTimestamp).toISOString().replace(/z|t/gi, ' ').trim() || null
    },

    /**
     * Shadow implementation of native mysql 
     * query to ensure connection is defined
     */
    query(query, params) {
        if (!this.connection) throw new Error('Missing DB connection. Please create connection with db.connect() first!');

        return this.connection.query(query, params);
    },

    // Private methods

    /**
     * Prepare sql query based on column name and value
     * 
     * @param {Object} obj Column:value pairs
     */
    prepareSelectQuery(obj, limit = 0) {
        const keys = Object.keys(obj);
        const values = Object.values(obj);

        const queryString = this.prepareSelectQueryString(keys.length, limit);
        const keyValuePairs = this.transformObjectToSearchArray(keys, values);

        return mysql
            .format(queryString, [this.tableName, ...keyValuePairs])
            .replace('= NULL', 'IS NULL');
    },

    /**
     * Prepare raw SQL query template optimized for better using of indices
     * 
     * @param {number} pairsNumber number of column:value pairs
     * @param {number} limit limit number of results
     */
    prepareSelectQueryString(pairsNumber, limit) {
        if (pairsNumber < 1) return;
        
        let query;
        if (pairsNumber == 1) {
            query = 'SELECT * FROM ?? WHERE ?? = ?';
        } else {
            query = 'SELECT * FROM ?? WHERE ?? = ?' + ' AND ?? = ?'.repeat(pairsNumber - 1);
        }

        if (limit > 0) {
            query += ' LIMIT ' + limit;
        }
        
        return query
    },

    /**
     * Tranform keys and values obtained from search 
     * object into array usable in mysql.prepare function
     * (e.g. {column1: value1, column2: value2} becomes [ column1, value1, column2, value2 ])
     * 
     * @param {Array} keys Keys array
     * @param {Array} values Values array
     * 
     * @returns {Array} [ key1, value1, key2, value2 ...]
     */
    transformObjectToSearchArray(keys, values) {
        return keys.reduce((combArr, elem, i) => combArr.concat(elem, values[i]), []);
    }, 

    async checkDatasourceCacheOrReturnQuery(query) {
        const redisKey = md5.YaMD5.hashStr(query);

        console.time(this.tableName + ': check cache');
        let cached = await this.redisClient.getValue(redisKey);
        if (cached) {
            cached = JSON.parse(cached);
            console.timeEnd(this.tableName + ': check cache');
            return cached;
        };

        console.time(this.tableName + ': sql queries');
        let nonCached = await this.query(query);
        console.timeEnd(this.tableName + ': sql queries');

        console.time(this.tableName + ': set cache');
        if (nonCached.length > 0) {
            await this.redisClient.setValue(redisKey, JSON.stringify(nonCached), 15 * 60);
        }
        console.timeEnd(this.tableName + ': set cache');

        return nonCached;
    }
}

module.exports = database;
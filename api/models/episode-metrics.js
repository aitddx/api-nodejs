const db = require('./index');
const mysql = require('mysql');
const Sanic = require('sanic.js').Library;
const { duration2seconds } = require('../helpers/time-conversions');
const md5 = require('yamd5.js');

let readable = ['user_id', 'session_id', 'platform_id', 'episode_id', 'adaptive_streaming', 'live', 'aborted_episode', 'initial_delay', 'stallings_count', 'stallings_average', 'outlier', 'timestamp'];
let writable = readable;

var episode_metrics = Object.assign(Object.create(db), {
    create,
    createFromPrepared,
    prepareForSave,

    getEpisodesById,
    getTotalEpisodes,
    getTotalEpisodesByType,
    
    callTestDb,

    getRawInitialDelay,
    getRawStallingNumber,
    getRawStallingDuration
});
episode_metrics.init('episode_metrics', readable, writable);

module.exports = episode_metrics;

/**
 * 
 * 
 * @param {*} request 
 */
async function getTotalEpisodes(request) {
    let query = `
        SELECT 
            cast(cast(UNIX_TIMESTAMP(timestamp)/(?) as signed) * ? as signed) as time, 
            COUNT(*) AS value,
            live
        FROM episode_metrics
        WHERE timestamp >= ? AND timestamp <= ?
        GROUP BY 1, live
        ORDER BY time ASC;`;
    let interval = duration2seconds((request.scopedVars.binning && request.scopedVars.binning.value) || '10m');    
    query = mysql.format(query, [interval, interval, request.range.from, request.range.to]);

    return await this.checkDatasourceCacheOrReturnQuery(query);
}

async function getTotalEpisodesByType(request, type = 1) {
    let query = `SELECT 
            cast(cast(UNIX_TIMESTAMP(timestamp)/(?) as signed) * ? as signed) as time, 
            COUNT(*) AS value
        FROM episode_metrics
        WHERE timestamp >= ? AND timestamp <= ? AND live = ?
        GROUP BY 1
        ORDER BY time ASC;`;
    let interval = duration2seconds((request.scopedVars.binning && request.scopedVars.binning.value) || '10m');
    query = mysql.format(query, [interval, interval, request.range.from, request.range.to, type]);

    return await this.checkDatasourceCacheOrReturnQuery(query);
}

async function getEpisodesById(request) {
    let q1 = `SELECT e.id as episode_id, episode_content_id
            FROM episodes as e
            LEFT JOIN episode_contents ON e.episode_detail_id = episode_contents.id
            WHERE episode_contents.episode_content_id IN (?);`;
    q1 = mysql.format(q1, [request.scopedVars.episode_id.value]);
    let episodeContentIds = await this.checkDatasourceCacheOrReturnQuery(q1);

    let q2 = `SELECT 
            cast(cast(UNIX_TIMESTAMP(timestamp)/(?) as signed) * ? as signed) as time, 
            COUNT(*) AS value,
            episode_id
        FROM episode_metrics
        WHERE timestamp >= ? AND timestamp <= ? AND episode_id IN (?)
        GROUP BY 1, episode_id
        ORDER BY time ASC;`;
    let interval = duration2seconds((request.scopedVars.binning && request.scopedVars.binning.value) || '10m');
    q2 = mysql.format(q2, [interval, interval, request.range.from, request.range.to, Sanic.Array.map(episodeContentIds, _ => _.episode_id)]);
    let episodeMetrics = await this.checkDatasourceCacheOrReturnQuery(q2);

    return [episodeContentIds, episodeMetrics];
}

function create(user_id, session_id, episode_id, platform_id, episode) {
    let model = this.prepareForSave(user_id, session_id, episode_id, platform_id, episode);

    return this.query(mysql.format(
        'INSERT INTO ?? (??) VALUES ?',
        [this.tableName, this.writableAttributes, [model]]
    ));
}

function createFromPrepared(episodes) {
    if (episodes.length === 0) return;

    return this.query(mysql.format(
        'INSERT INTO ?? (??) VALUES ?', 
        [ this.tableName, this.writableAttributes, episodes ]
    ));
}

function prepareForSave(user_id, session_id, episode_id, platform_id, episode) {
    let stallings = [];
    if (Array.isArray(episode.stallings)) {
        stallings = episode.stallings.map(stalling => 
            stalling.stalling_end - stalling.stalling_start
        );
    }

    let initial_delay = stallings.length > 0 ? stallings[0] : false;
    let stallings_count = stallings.length <= 1 ? 0 : stallings.length - 1;
    let stalling_avg = stallings.length > 1 ? stallings.slice(1)
        .reduce((prev, curr) => curr < 20000 ? prev + curr : prev, 0) / Math.max(stallings_count, 1) : null;
    let outlier = (initial_delay < 100000 && initial_delay >= 0 && stallings_count <= 15) ? 0 : 1;

    return [
        user_id, 
        session_id, 
        platform_id,        
        episode_id,
        episode.segment_id == '', // adaptive streaming if segment_id is empty then true
        episode.episode_length == 0, // live if episode_length == 0
        initial_delay === false, // aborted episode if stallings array is empty        
        initial_delay, // initial delay = first stalling in the array     
        stallings_count,
        stalling_avg,
        outlier,
        this.convertToMysqlTimestamp(episode.episode_start)
    ];
}

/**
 * Testing method for DB performance test
 */
function callTestDb() {
    var query = `SELECT id FROM episode_metrics LIMIT 1`;

    return this.query(query);
}

/**
 * 
 * 
 * @param {*} request 
 */
async function getRawInitialDelay(request, live = 0) {
    let query = `
        SELECT 
            UNIX_TIMESTAMP(timestamp) DIV ? * ? as time,
            initial_delay as value,
            sess.isp_id,
            sess.operating_system_id,
            sess.platform_id,
            sess.interface_type_id
        FROM episode_metrics
        INNER JOIN sessions as sess
        ON episode_metrics.\`session_id\` = sess.\`id\`
        WHERE outlier = 0 and live = ? AND timestamp BETWEEN ? AND ?;`
        .replace(/(\r\n|\n|\r)/gm,'').replace(/\s+/g, ' ');

    let interval = duration2seconds((request.scopedVars.binning && request.scopedVars.binning.value) || '10m');
    query = mysql.format(query, [interval, interval, live, request.range.from, request.range.to]);

    return await this.checkDatasourceCacheOrReturnQuery(query);
}

/**
 * 
 * 
 * @param {*} request 
 */
async function getRawStallingNumber(request, live = 0) {
    let query = `
        SELECT 
            UNIX_TIMESTAMP(timestamp) DIV ? * ? as time,
            stallings_count as value,
            sess.isp_id,
            sess.operating_system_id,
            sess.platform_id,
            sess.interface_type_id
        FROM episode_metrics
        INNER JOIN sessions as sess
        ON episode_metrics.\`session_id\` = sess.\`id\`
        WHERE outlier = 0 and live = ? AND timestamp BETWEEN ? AND ?;`
        .replace(/(\r\n|\n|\r)/gm,'').replace(/\s+/g, ' ');

    let interval = duration2seconds((request.scopedVars.binning && request.scopedVars.binning.value) || '10m');
    query = mysql.format(query, [interval, interval, live, request.range.from, request.range.to]);

    return await this.checkDatasourceCacheOrReturnQuery(query);
}

/**
 * 
 * 
 * @param {*} request 
 */
async function getRawStallingDuration(request, live = 0) {
    let query = `
        SELECT 
            UNIX_TIMESTAMP(timestamp) DIV ? * ? as time,
            stallings_average as value,
            sess.isp_id,
            sess.operating_system_id,
            sess.platform_id,
            sess.interface_type_id
        FROM episode_metrics
        INNER JOIN sessions as sess
        ON episode_metrics.\`session_id\` = sess.\`id\`
        WHERE outlier = 0 and live = ? AND timestamp BETWEEN ? AND ?;`
        .replace(/(\r\n|\n|\r)/gm,'').replace(/\s+/g, ' ');

    let interval = duration2seconds((request.scopedVars.binning && request.scopedVars.binning.value) || '10m');
    query = mysql.format(query, [interval, interval, live, request.range.from, request.range.to]);

    return await this.checkDatasourceCacheOrReturnQuery(query);
}
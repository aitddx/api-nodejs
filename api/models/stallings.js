const db = require('./index');
const mysql = require('mysql');

let readble = ['episode_id', 'order', 'stalling_start', 'duration'];
let writable = ['episode_id', 'order', 'stalling_start', 'duration'];

var stallings = Object.assign(Object.create(db), {
    create,
    createFromPrepared,
    prepareForSave
});
stallings.init('stallings', readble, writable);

module.exports = stallings;

async function create(episode_id, stallings) {
    if (!stallings) return;
    stallings = this.prepareForSave(episode_id, stallings);

    return await this.query(mysql.format(
        'INSERT INTO ?? (??) VALUES ?', 
        [ this.tableName, this.writableAttributes, stallings ]
    ));
}

async function createFromPrepared(stallings) {
    if (stallings.length === 0) return;

    return await this.query(mysql.format(
        'INSERT INTO ?? (??) VALUES ?', 
        [this.tableName, this.writableAttributes, stallings]
    ));
}

function prepareForSave(episode_id, stallings) {
    return stallings.map((stalling, idx) => [
        episode_id,
        idx,
        this.convertToMysqlTimestamp(stalling.stalling_start),
        stalling.stalling_end - stalling.stalling_start
    ]);
}
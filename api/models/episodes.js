const db = require('./index');
const mysql = require('mysql');
const episode_contents = require('./episode-contents');
const bitrates = require('./bitrates');
const stallings = require('./stallings');
const episode_metrics = require('./episode-metrics')

const readable = ['session_id', 'order', 'episode_detail_id', 'segment_id', 'episode_length', 'episode_start', 'episode_end'];
const writable = [...readable];

var episodes = Object.assign(Object.create(db), {
    addToSession,
    create,
    prepareForSave
});
episodes.init('episodes', readable, writable);
module.exports = episodes;

async function addToSession(user_id, session_id, platform_id, episodes) {
    if (!episodes) return;

    let order = 0;
    let bitratesPrepared = [];
    let stallingsPrepared = [];
    let episodeMetricsPrepared = [];
    for (const episode of episodes) {
        let res = await this.create(session_id, episode, order);
        // Do not store bitrates, stallings and metrics after every episode row records, but rather prepare it
        // for later use. This will save multiple INSERT queries, as multiple row can be inserted at single time once
        // they are prepared to be saved.
        if (typeof episode.bitrates !== 'undefined') {
            bitratesPrepared.push(...bitrates.prepareForSave(res.insertId, episode.bitrates));
        }
        if (typeof episode.stallings !== 'undefined') {
            stallingsPrepared.push(...stallings.prepareForSave(res.insertId, episode.stallings));
        }
        episodeMetricsPrepared.push(episode_metrics.prepareForSave(user_id, session_id, res.insertId, platform_id, episode));
        order += 1;
    }

    // Save prepared bitrates, stallings and episode_metrics data (this runs only 3 queries, instead of 3*episode_count)
    // By using Promise.all queries will run in parallel
    await Promise.all([
        bitrates.createFromPrepared(bitratesPrepared),
        stallings.createFromPrepared(stallingsPrepared),
        episode_metrics.createFromPrepared(episodeMetricsPrepared)
    ]);
}

async function create(session_id, episode, order) {
    let model = await this.prepareForSave(session_id, order, episode);

    return this.query(mysql.format(
        'INSERT INTO ?? (??) VALUES ?',
        [this.tableName, this.writableAttributes, [model]]
    ));
}

async function prepareForSave(session_id, order, episode) {
    let episode_detail_id = await episode_contents.findOrCreate({
        episode_content_id: episode.episode_id || "unknown", 
        episode_title: episode.episode_title || "unknown"
    });
    
    return [
        session_id, 
        order,
        episode_detail_id, 
        episode.segment_id || null,
        episode.episode_length,
        this.convertToMysqlTimestamp(episode.episode_start),
        this.convertToMysqlTimestamp(episode.episode_end)
    ]
}
const db = require('./index');
const mysql = require('mysql');

const isps = require('./isps');
const devices = require('./device-types');
const interfaces = require('./interface-types');
const operating_systems = require('./operating-systems');
const client_versions = require('./client-versions');
const episodes = require('./episodes');
const actions = require('./actions');
const platforms = require('./platforms');

const readable = [
    'user_id',
    'isp_id',
    'device_type_id',
    'operating_system_id',
    'interface_type_id',
    'client_version_id',
    'platform_id',
    'session_start',
    'session_end'
];
const writable = [...readable];

var sessions = Object.assign(Object.create(db), {
    create,
    get,
    getByTimestamp,
    isAlreadyRecorded,
    prepareForSave,
    store
});
sessions.init('sessions', readable, writable);
module.exports = sessions;

async function get() {
    return this.query(mysql.format(
        'SELECT * FROM ?? LIMIT 1', [this.tableName]));
}

/**
 * Get sessions for given user and given session_start timestamp.
 * If we receive any records, that means that this session has already been recorded
 * 
 * @param {int} user_id 
 * @param {int} unix_timestamp 
 */
async function getByTimestamp(user_id, unix_timestamp) {
    let session_start = this.convertToMysqlTimestamp(unix_timestamp);

    return this.query(mysql.format(
        'SELECT ?? FROM ?? WHERE session_start = ? AND user_id = ?', 
        [this.readableAttributes, this.tableName, session_start, user_id]
    ));
}

async function isAlreadyRecorded(user_id, unix_timestamp, platform = 'unknown') {
    const res = await this.getByTimestamp(user_id, unix_timestamp);
    const platform_id = platforms.findOrCreate({ name: platform });
    const rowsWithGivenPlatformID = res.filter(row => row.platform_id == platform_id);
    
    return rowsWithGivenPlatformID.length > 0;
}

async function store(user_id, session) {
    try {
        await this.connection.beginTransaction();
        const platform_id = platforms.findOrCreate({ name: session.platform });
        let session_row = await this.create(user_id, platform_id, session);
        await Promise.all([
            episodes.addToSession(user_id, session_row.insertId, platform_id, session.episodes),
            actions.addToSession(session_row.insertId, session.actions)
        ]);
        await this.connection.commit();

        return session_row.insertId;
    } catch (error) {
        await this.connection.rollback();
        throw error;
    }
}

async function create(user_id, platform_id, session) {
    let model = await this.prepareForSave(user_id, platform_id, session);

    return this.query(mysql.format(
        'INSERT INTO ?? (??) VALUES ?', 
        [ this.tableName, this.writableAttributes, [model] ]
    ));
}

async function prepareForSave(user_id, platform_id, session) {
    // Run findOrCreate promises in parallel, rather then using 'await' for every single one to finish
    const isp = isps.findOrCreate({
        name: session.ISP || 'unknown'
    });
    const device_type = devices.findOrCreate({
        type: session.device_type || 'unknown',
        platform_id: platform_id
    });
    const operating_system = operating_systems.findOrCreate({
        name: session.os_version || 'unknown',
        platform_id: platform_id
    });
    const interface_type = interfaces.findOrCreate({
        type: session.interface || 'unknown'
    });
    const client_version = client_versions.findOrCreate({
        version: session.client_version || 'unknown',
        platform_id: platform_id
    });
    const session_start = this.convertToMysqlTimestamp(session.session_start);
    const session_end = this.convertToMysqlTimestamp(session.session_end);

    return [
        user_id,
        await isp,
        await device_type,
        await operating_system,
        await interface_type,
        await client_version,
        platform_id,
        session_start,
        session_end
    ];
}
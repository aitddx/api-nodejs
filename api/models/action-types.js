const db = require('./index');
const mysql = require('mysql');

let readable = ['id', 'type'];
let writable = ['id', 'type'];

var action_types = Object.assign(Object.create(db), {
    getFromArray
});
action_types.init('action_types', readable, writable);

module.exports = action_types;

function getFromArray(values) {
    let query = mysql.format(
        'SELECT ?? FROM ?? WHERE `type` IN ? ORDER BY FIELD (`type`, ?)', 
        [this.readableAttributes, this.tableName, [values], values]
    );

    return this.query(query);
}
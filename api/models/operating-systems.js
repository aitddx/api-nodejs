const db = require('./index');
const mysql = require('mysql');
const md5 = require('yamd5.js');

var operating_systems = Object.assign(Object.create(db), {
    getTopOS
});
operating_systems.init('operating_systems');

async function getTopOS(from, to, count = 10) {
    let query = `SELECT s.operating_system_id, o.\`name\` as \`name\`
        FROM sessions as s
        JOIN operating_systems as o
        ON s.operating_system_id = o.id
        WHERE s.session_start BETWEEN ? AND ?
        GROUP BY s.operating_system_id
        ORDER BY count(s.operating_system_id) DESC
        LIMIT ?;`.replace(/(\r\n|\n|\r)/gm, '').replace(/\s+/g, ' ');
    query = mysql.format(query, [from, to, count]);

    return await this.checkDatasourceCacheOrReturnQuery(query);
}

module.exports = operating_systems;
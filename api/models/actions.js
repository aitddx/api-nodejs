const action_types = require('./action-types');
const db = require('./index');
const mysql = require('mysql');

const readable = ['session_id', 'action_type_id', 'timestamp'];
const writable = [...readable];

var actions = Object.assign(Object.create(db), {
    addToSession,
    create,
    prepareForSave
});
actions.init('actions', readable, writable);
module.exports = actions;

async function addToSession(session_id, actions) {
    if (!actions) return;

    return this.create(
        await this.prepareForSave(session_id, actions)
    );
}

function create(actions) {
    return this.query(mysql.format(
        'INSERT INTO ?? (??) VALUES ?', 
        [this.tableName, this.writableAttributes, actions]
    ));
}

async function prepareForSave(session_id, actions) {
    // Get unique action names
    let actionList = actions
        .map(a => a.action)
        .filter((value, index, self) => self.indexOf(value) === index);
    
    // Check all records which already exists in DB with given action names
    let inDB = await action_types.getFromArray(actionList);
    // Get all unique missing action types
    let notInDB = actionList.filter(action => inDB.map(_ => _.type).indexOf(action) < 0);

    // Record missing action types
    let dbPromises = notInDB.map(async action => {
        let row = await action_types.create({type: action});
        return { id: row.insertId, type: action };
    });
    // Execute promises in parallel (other way to do this would be with 'await Promise.all()')
    // See: https://developers.google.com/web/fundamentals/primers/async-functions
    for (const dbPromise of dbPromises) {
        let row = await dbPromise;
        inDB.push(row);
    }

    return actions.map(action => {
        let row = inDB.filter(_ => _.type === action.action)[0];

        return [
            session_id,
            row.id,
            this.convertToMysqlTimestamp(action.timestamp)
        ];
    });
}
module.exports = {
    duration2seconds
}

/**
 * Convert duration string to seconds
 * 
 * @param {string} duration string like '5m', '1d' etc.
 */
function duration2seconds(duration) {
    if (typeof duration !== "string") return;
    const unit = duration.slice(-1);
    let value = duration.slice(0, -1);

    switch (unit) {
        case "m":
            value = value * 60;
            break;
        case "h":
            value = value * 60 * 60;
            break;
        case "d":
            value = value * 60 * 60 * 24;
            break;
        default:
            break;
    }

    return value;
}
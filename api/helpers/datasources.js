const { logger } = require('../middlewares/logger');
const Sanic = require('sanic.js').Library;
const isps = require('../models/isps');
const oSystems = require('../models/operating-systems');
const iTypes = require('../models/interface-types');

module.exports = {
    getAvgPerISP,
    getAvgPerOS,
    getAvgPerInterface,
    roundTimeRangeForBetterCacheUtilization,
}

/**
 * Get Avg value per ISP
 * 
 * @param {object} req Request
 * @param {function} getRawResultsQuery function for retrieving raw row data from episode_metrics table
 * @param {number 0|1} live flag for distinction between live and VoD data
 */
async function getAvgPerISP(req, getRawResultsQuery, live = 1) {
    const topIspCount = +req.body.scopedVars.isp_top.value || 10;
    const minCountThreshold = req.body.scopedVars.binNth && req.body.scopedVars.binNth.value;

    let rows = await getRawResultsQuery(req.body, live);
    let topISPs = await isps.getTopIsps(req.body.range.from, req.body.range.to, topIspCount);

    return calculateTargetsAndDatapoints(rows, topISPs, 'isp_id', minCountThreshold, 'avg');
}

/**
 * Get Avg value per OS version
 * 
 * @param {object} req Request
 * @param {function} getRawResultsQuery function for retrieving raw row data from episode_metrics table
 * @param {number 0|1} live flag for distinction between live and VoD data
 */
async function getAvgPerOS(req, getRawResultsQuery, live = 1) {
    const topOsCount = +req.body.scopedVars.os_top.value || 10;
    const minCountThreshold = req.body.scopedVars.binNth && req.body.scopedVars.binNth.value;

    let rows = await getRawResultsQuery(req.body, live);
    let topOSs = await oSystems.getTopOS(req.body.range.from, req.body.range.to, topOsCount);

    return calculateTargetsAndDatapoints(rows, topOSs, 'operating_system_id', minCountThreshold, 'avg');
}

/**
 * Get Avg value delay per Interface
 * 
 * @param {object} req Request
 * @param {function} getRawResultsQuery function for retrieving raw row data from episode_metrics table
 * @param {number 0|1} live flag for distinction between live and VoD data
 */
async function getAvgPerInterface(req, getRawResultsQuery, live = 1) {
    const minCountThreshold = req.body.scopedVars.binNth && req.body.scopedVars.binNth.value;

    let rows = await getRawResultsQuery(req.body, live);
    let topInterfaces = await iTypes.getTopInterfaces(req.body.range.from, req.body.range.to);

    return calculateTargetsAndDatapoints(rows, topInterfaces, 'interface_type_id', minCountThreshold, 'avg');
}

/**
 * From raw episode metrics rows, calculate statistical aggregate based on selected property
 * 
 * @param {array} rows episode_metrics rows
 * @param {array} tops Top x values for given property (ISP, OS, Interface, etc.)
 * @param {string} property Property to be used for calculating statistical aggregate
 * @param {number} minCountThreshold Minimum binning size (from GRafana Dashboard)
 * @param {string} aggregate SUM|COUNT|AVG
 */
function calculateTargetsAndDatapoints(rows, tops, property, minCountThreshold, aggregate = 'sum') {
    rows = Sanic.Array.filter(rows, _ => tops.map(_ => _[property]).includes(_[property]));

    console.time('sanic')
    let res = calculateAggregateFromProperty(rows, property, minCountThreshold, aggregate);
    const mapped = Object.keys(res).map(_ => ({
        target: tops.filter(isp => isp[property] == _)[0].name,
        datapoints: Object.keys(res[_]).map(key => [res[_][key], key * 1000])
    }));
    console.timeEnd('sanic');

    return mapped;
}

/**
 * From raw episode metrics data, calculate given 
 * aggregate (statistic value: avg, sum or count) from a
 * property name like: operating_system, os_version, isp, etc...
 * 
 * @param {object[]} arr Episode_metrics raw rows
 * @param {string} property target property name on which an aggregate (sum, count, avg) should be calculated
 * @param {number} minCountThreshold Minimum binning size (filter limit threshold)
 * @param {string} aggregate Statistical aggregate, possible values: sum, count, avg
 */
function calculateAggregateFromProperty(arr, property, minCountThreshold = 0, aggregate = 'sum') {
    if (!Array.isArray(arr))
        throw new PropertyAggregateCalculationError('Input is not an Array of objects');
    if (typeof arr[0] !== 'object')
        throw new PropertyAggregateCalculationError('Input is not an Array of objects');
    if (typeof property !== 'string')
        throw new PropertyAggregateCalculationError('Input is not a string');
    if (!arr[0].hasOwnProperty(property))
        throw new PropertyAggregateCalculationError('Property does not exists in given object');
    if (aggregate != 'sum' && aggregate != 'count' && aggregate != 'avg')
        throw new PropertyAggregateCalculationError('Aggregate is not sum, count or avg');

    let res = {};
    arr.reduce((p, c) => {
        var time = c.time;
        if (!p.hasOwnProperty(c[property])) {
            p[c[property]] = {};
            // Save to final object
            res[c[property]] = {};
        }
        if (!p[c[property]].hasOwnProperty(time)) {
            p[c[property]][time] = {
                sum: c.value,
                count: 1,
                avg: c.value
            };
            // Save to final object
            res[c[property]][time] = c.value;

            return p;
        }
        const currentCount = p[c[property]][time]['count'] + 1;
        const currentAvg = ((currentCount - 1) * p[c[property]][time]['avg'] + c.value) / currentCount;
        p[c[property]][time] = {
            sum: p[c[property]][time]['sum'] + c.value,
            count: currentCount,
            avg: currentAvg
        };
        // Save to final object
        res[c[property]][time] =
            p[c[property]][time]['count'] <= minCountThreshold ?
            null :
            p[c[property]][time][aggregate];
        // if (time == 1547985600 && c[property] == 4051) console.log(c[property], res[c[property]][time]);
        // if (time == 1547989200 && c[property] == 4051) console.log(c[property], res[c[property]][time]);

        return p;
    }, {});

    return res;
}

/**
 * Alter from and to time value, so they are rounded to 
 * nearest lower time-interval value, so that the cache is
 * better utilized for query like "last n-days", "last n-hours", etc.
 * 
 * @param {object} req Request object
 */
function roundTimeRangeForBetterCacheUtilization(req) {
    logger.debug('incoming times: ', req.body.range.from, req.body.range.to);
    let from = roundTimeTo(new Date(req.body.range.from), 15);
    let to = roundTimeTo(new Date(req.body.range.to), 15);
    logger.debug('rounded times: ', from, to);

    req.body.range.from = from;
    req.body.range.to = to;

    return req;
}

/**
 * Round to to lowest nearest (minutes) value
 * 
 * @param {Date} date Javascript Date object
 * @param {number} minutes interval to which the time should be rounded
 */
function roundTimeTo(date, minutes = 15) {
    let ms = 1000 * 60 * minutes; // convert minutes to ms
    return new Date(Math.floor(date.getTime() / ms) * ms);
}

/**
 * Custom error message
 * 
 * @param {string} message Error message
 */
function PropertyAggregateCalculationError(message) {
    this.name = 'PropertyAggregateCalculationError';
    this.message = message;
    this.stack = (new Error()).stack;
}
PropertyAggregateCalculationError.prototype = new Error;
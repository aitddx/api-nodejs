const path = require('path');
const pino = require('pino');
const fs = require('fs');

const logDir = path.resolve(process.cwd(), './logs');
const logPath = path.resolve(process.cwd(), './logs/errors.log');

openOrCreateDir(logDir);
openOrCreateFile(logPath);

const logger = pino({ level: 'error' });
const loggerFile = pino(pino.extreme(logPath));

function openOrCreateDir(path) {
    try {
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path)
        }
    } catch (err) {
        console.log('Error occured while creating log dir');
        console.error(err);
        process.exit(1);
    }
}

function openOrCreateFile(filename) {
    try {
        let opened = fs.openSync(filename, 'a')
        fs.closeSync(opened);
    } catch (err) {
        console.log('Error occured while creating log file');
        console.error(err);
        process.exit(1);
    }
}

module.exports.logger = logger;
module.exports.loggerFile = loggerFile;
/**
 * Based on:
 * https: //nemethgergely.com/error-handling-express-async-await/
 * https: //itnext.io/using-async-await-to-write-cleaner-route-handlers-7fc1d91b220b
 * also based on: https: //youtu.be/DwQJ_NPQWWo?t=720
 * 
 * If you’ d like to use async -await inyour Express route handlers, you have to do the following:
 * 1. create a wrapper for the async functions, so errors are propagated
 * 2. instead of calling next in your route handlers, simply throw.
 */
const catchErrors = promiseFn => (req, res, next) => 
    promiseFn(req, res, next)
    .catch(next);

module.exports = catchErrors;
const bluebird = require("bluebird");
const redis = require('redis');

// USe redis with async functions
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

var client = {
    init() {
        this.client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);

        //Incase any error pops up, log it
        this.client.on("error", function (err) {
            console.log("Error " + err);
        });

        return this;
    },

    async setValue(key, value, expireIn = 30*60) {
        return await this.client.setAsync(key, value, 'EX', expireIn);
    },

    async getValue(key) {
        return await this.client.getAsync(key);
    },

    async delValue(key) {
        return await this.client.delAsync(key);
    }
}

module.exports = client;
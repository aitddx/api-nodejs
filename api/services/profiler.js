const fs = require('fs');
const profiler = require('v8-profiler-node8')
const { logger } = require('../middlewares/logger');

let id;

function start(name) {
    id = name + '_' + Date.now() + '.cpuprofile';
    profiler.startProfiling(id);
}

function stop() {
    // stop profiling in n seconds and exit
    const profile = JSON.stringify(profiler.stopProfiling(id))
    fs.writeFile(`${process.cwd()}/profiles/${id}`, profile, () => {
        console.log('Profiling done, written to:', id);
    })
}

module.exports = {
    start: start,
    stop: stop
}
require('dotenv').config();
const MongoClient = require('mongodb').MongoClient;

var dbInstance;

var connect = () => {
    return new Promise((resolve, reject) => {
        if (dbInstance) return resolve(dbInstance);

        let url = `mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@mongo:27017/${process.env.MONGO_INITDB_DATABASE}?authMechanism=DEFAULT&authSource=admin`;

        // https://mongodb.github.io/node-mongodb-native/driver-articles/mongoclient.html
        const mongoOptions = {
            poolSize: 20,
            auto_reconnect: true,
            useNewUrlParser: true
        };

        MongoClient.connect(url, mongoOptions, function (err, database) {
            if (err) return reject(err);

            dbInstance = database;
            // the Mongo driver recommends starting the server here because most apps *should* fail to start if they have no DB.  If yours is the exception, move the server startup elsewhere. 
            return resolve(dbInstance);
        });
    });

};

module.exports.connect = connect;
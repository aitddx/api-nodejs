const express = require('express');
const pino = require('pino');

const authMiddleware = require('./api/middlewares/auth');
const bodyParser = require('body-parser');
const cors = require('./api/middlewares/cors');
const { logger, loggerFile } = require('./api/middlewares/logger');

const apiTestRoutes = require('./api/routes/api-tests');
const cronTestRoutes = require('./api/routes/cron-tests');
const dsAvgInitialDelayRoutes = require('./api/routes/ds-avg-initial-delay');
const dsAvgStallingRoutes = require('./api/routes/ds-avg-stalling');
const dsEpisodesRoutes = require('./api/routes/ds-episodes');
const feedRoutes = require('./api/routes/feeds');
const sessionV1Routes = require('./api/routes/sessions.v1');
const sessionV2Routes = require('./api/routes/sessions.v2');
const userRoutes = require('./api/routes/users');

// Listen for errors before app starts
// process.on('uncaughtException', pino.final(logger, (err, finalLogger) => {
//     finalLogger.error(err);
//     process.exit(1);
// }));

// @TODO: Fix pino.final logger while using pm2 
process.on('uncaughtException', err => {
    console.log(err);
    process.exit(1);
});

// App container
const app = express();

// Middlewares
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors);

// Legacy Routes for old iOS app
app.use('/api/v1/session', authMiddleware, sessionV1Routes);
app.use('/api/v1/auth', userRoutes);

// V2 API routes
app.use('/api/v2/sessions', authMiddleware, sessionV2Routes);
app.use('/api/v2/auth', userRoutes);
app.use('/api/v2/feeds', feedRoutes);

// SimpleJSON Datasources for Grafana
app.use('/api/ds/episodes', dsEpisodesRoutes);
app.use('/api/ds/avg-initial-delay', dsAvgInitialDelayRoutes);
app.use('/api/ds/avg-stalling', dsAvgStallingRoutes);

// Tests and performance measurement EPs
app.use('/api/tests', apiTestRoutes);
app.use('/api/cron-tests', cronTestRoutes);

// Unknown routes middleware
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

// Default error handler
app.use((error, req, res, next) => {
    logger.error(error);
    loggerFile.error(error.message);
    res.status(error.status || 500);
    res.json({ error: { message: error.message } });
});

module.exports = app; 

'use strict';
require('dotenv').config();
const db = require('../api/models');

async function run() {
    const activeConnection = await db.connect();

    console.time('Precalculating episode metric results');
    await precalculateResults();
    console.timeEnd('Precalculating episode metric results');

    await activeConnection.release();
}

function precalculateResults() {
    return db.query(`call updateLiveOverview(14, 2, 300)`);
}

module.exports = run;

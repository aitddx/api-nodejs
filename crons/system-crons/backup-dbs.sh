#!/bin/bash
cd /home/vh795537/qoestream/

docker-compose -f docker-compose.production.yml stop

docker run --rm --volumes-from qs_prod_mysql -v $(pwd)/backups:/backups ubuntu tar cvf /backups/qs-mysql.tar /var/lib/mysql

docker run --rm --volumes-from qs_prod_mongo -v $(pwd)/backups:/backups ubuntu tar cvf /backups/qs-mongo.tar /data/db

docker-compose -f docker-compose.production.yml up -d
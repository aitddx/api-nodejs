'use strict';
require('dotenv').config();
const db = require('../api/models');

async function run() {
    const activeConnection = await db.connect();

    console.time('Optimize process time');
    await optimizeTables();
    console.timeEnd('Optimize process time');

    await activeConnection.release();
}

function optimizeTables() {
    return db.query(`
        OPTIMIZE TABLE stallings;
        OPTIMIZE TABLE bitrates;
        OPTIMIZE TABLE actions;
        OPTIMIZE TABLE episode_metrics;
        OPTIMIZE TABLE episodes;        
        OPTIMIZE TABLE sessions;
    `);
}

module.exports = run;

'use strict';
require('dotenv').config();
const mysql = require('mysql');
const db = require('../api/models');

const sessionsHistoryInDays = 89;

async function run() {
    const activeConnection = await db.connect();

    let sessionIds = await getExpiredSessionIds(sessionsHistoryInDays).map(_ => _.id);

    console.log('sessions to be deleted: ', sessionIds.length);

    console.time('Delete process time');
    // Process session related tables
    await deleteExpiredSessions(sessionsHistoryInDays);
    await deleteExpiredEpisodes();
    await deleteExpiredEpisodeMetrics();
    await deleteExpiredActions();
    await deleteExpiredBitrates();
    await deleteExpiredStallings();
    console.timeEnd('Delete process time');

    await activeConnection.release();

    return sessionIds.length;
}

function getExpiredSessionIds(daysToPersist = 90) {
    return db.query(mysql.format(
        'SELECT s.id FROM sessions s WHERE session_end < DATE_SUB(NOW(), INTERVAL ? DAY)', daysToPersist
    ));
}

function deleteExpiredSessions(daysToPersist = 90) {
    console.log('Prunning sessions');
    return db.query(mysql.format(
        'DELETE s.* FROM sessions s WHERE session_end < DATE_SUB(NOW(), INTERVAL ? DAY)', daysToPersist
    ));
}

function deleteExpiredEpisodes() {
    console.log('Prunning episodes');
    return db.query(
        'DELETE e.* from episodes e LEFT JOIN sessions s ON e.session_id = s.id WHERE s.id IS NULL;'
    );
}

function deleteExpiredEpisodeMetrics() {
    console.log('Prunning episode_metrics');
    return db.query(
        'DELETE em.* from episode_metrics em LEFT JOIN sessions s ON em.session_id = s.id WHERE s.id IS NULL'
    );
}

function deleteExpiredActions() {
    console.log('Prunning actions');
    return db.query(
        'DELETE a.* from actions a LEFT JOIN sessions s ON a.session_id = s.id WHERE s.id IS NULL'
    );
}

function deleteExpiredBitrates() {
    console.log('Prunning bitrates');
    return db.query(
        'DELETE b.* from bitrates b LEFT JOIN episodes e ON b.episode_id = e.id WHERE e.id IS NULL'
    );
}

function deleteExpiredStallings() {
    console.log('Prunning stallings');
    return db.query(
        'DELETE s.* from stallings s LEFT JOIN episodes e ON s.episode_id = e.id WHERE e.id IS NULL'
    );
}

module.exports = run;

var CronJob = require('cron').CronJob;

const pruneOldData = require('./prune-old-data');
const pruneOldDataInBatches = require('./prune-old-data-in-batches');
const optimizeMysqlTables = require('./optimize-mysql-tables');
const precalculateEpisodeMetrics = require('./precalculate-episode-metrics');

const every30Seconds = '*/30 * * * * *';
const everyMinute = '0 */1 * * * *';
const everyTwoMinutes = '0 */2 * * * *';
const everyDayAtThree = '0 0 3 * * *';
const everyDayAtThreeThirty = '0 30 3 * * *';
const everyFirstDayOfMonthAtTwo = '0 0 2 1 * *';
const everyFirstDayOfMonthAtTwoThirty = '0 30 2 1 * *';

// Run imidiatelly:
pruneOldDataInBatchesCron(everyDayAtThree);
// pruneOldDataCron(everyFirstDayOfMonthAtTwo);
// optimizeMysqlTablesCron(everyFirstDayOfMonthAtTwoThirty);
precalculateEpisodeMetricsCron(everyDayAtThreeThirty);

// Or use in other modules:
module.exports = {
    pruneOldDataCron,
    pruneOldDataInBatchesCron,
    optimizeMysqlTablesCron,
    precalculateEpisodeMetricsCron
}

function pruneOldDataCron(when) {
    return new CronJob(when, function () {
        let time = new Date();
        console.log('START PRUNING OLD DATABASE DATA. START: ', time);
        pruneOldData().then(res => {
                console.log('Sessions deleted: ', res);
                console.log('SUCCESSFULL PRUNE OF OLD DATABASE DATA');
            })
            .catch(err => {
                console.error(err);
                process.exit(1);
            });
    }, null, true, 'Europe/Vienna');
}

function pruneOldDataInBatchesCron(when) {
    return new CronJob(when, function () {
        let time = new Date();
        console.log('START PRUNING OLD DATABASE DATA', time);
        pruneOldDataInBatches().then(res => {
                console.log('Sessions deleted: ', res);
                console.log('SUCCESSFULL PRUNE OF OLD DATABASE DATA');
                // process.exit();
            })
            .catch(err => {
                console.error(err);
                process.exit(1);
            });
    }, null, true, 'Europe/Vienna');
}

function optimizeMysqlTablesCron(when) {
    return new CronJob(when, function () {
        let time = new Date();
        console.log('START OPTIMIZING MYSQL TABLES. START: ', time);
        optimizeMysqlTables()
            .then(() => {
                console.log('SUCCESSFULL OPTIMIZING MYSQL TABLES');
            })
            .catch(err => {
                console.error(err);
                process.exit(1);
            });
    }, null, true, 'Europe/Vienna');
}

function precalculateEpisodeMetricsCron(when) {
    return new CronJob(when, function () {
        let time = new Date();
        console.log('START PRECALCULATING EPISODE METRICS. START: ', time);
        precalculateEpisodeMetrics().then(() => {
                console.log('SUCCESSFULL PRECALCULATING EPISODE METRICS');
            })
            .catch(err => {
                console.error(err);
                process.exit(1);
            });
    }, null, true, 'Europe/Vienna');
}
'use strict';
require('dotenv').config();
const mysql = require('mysql');
const db = require('../api/models');

const batchSize = 25000;
const sessionsHistoryInDays = 90;
const sessionIdsRedisKey = 'SessionIdsRedisKey';
const episodeIdsRedisKey = 'EpisodeIdsRedisKey';

async function run() {
    const activeConnection = await db.connect();

    let sessionIds = await getExpiredSessionIds(sessionsHistoryInDays).map(_ => _.id);
    let episodeIds = await getExpiredEpisodeIds(sessionIds).map(_ => _.id);
    [sessionIds, episodeIds] = await checkRedisForFailedDeleteAttempts(sessionIds, episodeIds);

    console.log('sessions to be deleted: ', sessionIds.length);
    console.log('episodes to be deleted: ', episodeIds.length)

    // Process session related tables
    await pruneSessionRelatesTables(sessionIds, batchSize);
    // Process episode related tables
    await pruneEpisodeRelatedTables(episodeIds, batchSize);

    await activeConnection.release();

    return sessionIds.length;
}

function getExpiredSessionIds(daysToPersist = 90) {
    return db.query(
        'SELECT s.id FROM sessions s WHERE session_end < DATE_SUB(NOW(), INTERVAL ' + daysToPersist + ' DAY)'
    );
}

/**
 * Find episodes which are related to expired sessions
 * 
 * @param {array} sessionIds Session ids which will be deleted
 */
function getExpiredEpisodeIds(sessionIds) {
    if (sessionIds === undefined || sessionIds.length === 0) return [];
    return db.query(mysql.format(
        'SELECT e.id FROM episodes e WHERE session_id IN (?)',
        [sessionIds]
    ));
}

/**
 * Delete all data related to expired sessions
 * 
 * @param {array} sessionIds Session IDs which should be deleted
 * @param {number} batchSize Number of rows to delete in single step
 */
async function pruneSessionRelatesTables(sessionIds, batchSize = 10000) {
    let s = 0;
    const sMax = sessionIds.length / batchSize;

    for (; s < sMax; s++) {
        let ids = sessionIds.slice(s * batchSize, (s + 1) * batchSize);
        console.log(`Deleting sessions batch ${s+1}/${Math.ceil(sMax)}`);
        console.time('sessions delete');
        await db.query(mysql.format('DELETE FROM sessions WHERE id IN (?)', [ids]));
        await db.query(mysql.format('DELETE FROM episode_metrics WHERE session_id IN (?)', [ids]));
        await db.query(mysql.format('DELETE FROM actions WHERE session_id IN (?)', [ids]));
        // [sessions, ems, actions] = await Promise.all([sessions, ems, actions]);

        // Put remaining ids to Redis cache
        const remainder = sessionIds.slice((s + 1) * batchSize);
        await db.redisClient.setValue(sessionIdsRedisKey, JSON.stringify(remainder));

        console.timeEnd('sessions delete');
    }
}

/**
 * Delete all data related to expired episodes
 * 
 * @param {array} sessionIds Episode IDs which should be deleted
 * @param {number} batchSize Number of rows to delete in single step
 */
async function pruneEpisodeRelatedTables(episodeIds, batchSize = 10000) {
    let e = 0;
    const eMax = episodeIds.length / batchSize;

    for (; e < eMax; e++) {
        let ids = episodeIds.slice(e * batchSize, (e + 1) * batchSize);

        console.log(`Deleting episodes batch ${e+1}/${Math.ceil(eMax)}`);
        console.time('episodes delete');
        await db.query(mysql.format('DELETE FROM episodes WHERE id IN (?)', [ids]));
        await db.query(mysql.format('DELETE FROM bitrates WHERE episode_id IN (?)', [ids]));
        await db.query(mysql.format('DELETE FROM stallings WHERE episode_id IN (?)', [ids]));
        // [episodes, bitrates, stallings] = await Promise.all([episodes, bitrates, stallings]);

        // Put remaining ids to Redis cache
        const remainder = episodeIds.slice((e + 1) * batchSize);
        await db.redisClient.setValue(episodeIdsRedisKey, JSON.stringify(remainder));

        console.timeEnd('episodes delete');
    }
}

/**
 * Check redis if there are some unprocessed session or episode IDs
 * If they are, combine them with latest IDs 
 * 
 * @param {array} sessionIds 
 * @param {array} episodeIds 
 */
async function checkRedisForFailedDeleteAttempts(sessionIds, episodeIds) {

    let unprocessedSessionsFromPrevious = db.redisClient.getValue(sessionIdsRedisKey);
    let unprocessedEpisodesFromPrevious = db.redisClient.getValue(episodeIdsRedisKey);

    [unprocessedSessionsFromPrevious, unprocessedEpisodesFromPrevious] = await Promise.all([
        unprocessedSessionsFromPrevious,
        unprocessedEpisodesFromPrevious
    ]);

    let sessionIdsCombined = combineUnprocessedAndCurrentSessions(unprocessedSessionsFromPrevious, sessionIds);
    let episodeIdsCombined = combineUnprocessedAndCurrentEpisodes(unprocessedEpisodesFromPrevious, episodeIds);

    await Promise.all([
        db.redisClient.setValue(sessionIdsRedisKey, JSON.stringify(sessionIdsCombined)),
        db.redisClient.setValue(episodeIdsRedisKey, JSON.stringify(episodeIdsCombined))
    ]);

    return [sessionIdsCombined, episodeIdsCombined];
}

/**
 * 
 * @param {array} unprocessedSessionsFromPrevious 
 * @param {*} sessionIds 
 */
function combineUnprocessedAndCurrentSessions(unprocessedSessionsFromPrevious, sessionIds) {
    if (!unprocessedSessionsFromPrevious) return sessionIds;
    
    unprocessedSessionsFromPrevious = JSON.parse(unprocessedSessionsFromPrevious);
    if (!Array.isArray(unprocessedSessionsFromPrevious)) {
        unprocessedSessionsFromPrevious = [unprocessedSessionsFromPrevious];
    }

    console.log('Unprocessed sessions after last failed run: ', unprocessedSessionsFromPrevious.length);
    if (sessionIds !== undefined && sessionIds.length > 0) {
        return [...unprocessedSessionsFromPrevious, ...sessionIds];
    }

    return unprocessedSessionsFromPrevious
}

function combineUnprocessedAndCurrentEpisodes(unprocessedEpisodesFromPrevious, episodeIds) {
    if (!unprocessedEpisodesFromPrevious) return episodeIds;
    
    unprocessedEpisodesFromPrevious = JSON.parse(unprocessedEpisodesFromPrevious);
    if (!Array.isArray(unprocessedEpisodesFromPrevious)) {
        unprocessedEpisodesFromPrevious = [unprocessedEpisodesFromPrevious];
    }

    console.log('Unprocessed episodes after last failed run: ', unprocessedEpisodesFromPrevious.length);
    if (episodeIds !== undefined && episodeIds.length > 0) {
        return episodeIds = [...unprocessedEpisodesFromPrevious, ...episodeIds];
    } 

    return unprocessedEpisodesFromPrevious;
}

module.exports = run;

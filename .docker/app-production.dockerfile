FROM node:10.15

# Create app directory
WORKDIR /var/www

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

### @TODO Use pm2 official docker container
### See: https://pm2.io/doc/en/runtime/integration/docker
RUN npm install pm2 -g

# RUN npm install 
# If you are building your code for production
RUN npm install --only=production

# Bundle app source
COPY . .
COPY ./.env.production /var/www/.env

EXPOSE 3001

CMD [ "npm", "run", "start:production" ]

### Initialize DB with this command
### docker-compose exec app ./node_modules/.bin/db-migrate up

### For testing DB run:
### docker-compose exec app ./node_modules/.bin/db-migrate db:create qoestream_test
### docker-compose exec app ./node_modules/.bin/db-migrate up --env test

### For running tests (first create qoestream_test db) 
### docker-compose exec app yarn run test
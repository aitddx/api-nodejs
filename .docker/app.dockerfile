FROM node:10.15

# Create app directory
WORKDIR /var/www

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./
RUN npm install v8-profiler-node8
RUN npm install

### Initialize DB with this command
### docker-compose exec app ./node_modules/.bin/db-migrate up

### For testing DB run:
### docker-compose exec app ./node_modules/.bin/db-migrate db:create qoestream_test
### docker-compose exec app ./node_modules/.bin/db-migrate up --env test

### For running tests (first create qoestream_test db) 
### docker-compose exec app yarn run test
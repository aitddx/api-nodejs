FROM nginx:1.10

ADD .docker/self-certs/server.crt /etc/nginx/certs/server.crt
ADD .docker/self-certs/server.key /etc/nginx/certs/server.key

ADD .docker/vhost.conf /etc/nginx/conf.d/default.conf

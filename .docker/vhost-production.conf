upstream qoestream_v2_api {
    least_conn;
	server app:3001 weight=10 max_fails=3 fail_timeout=30s;
}

upstream qoestream_v2_grafana {
	server grafana:3000;
}

# Redirect all HTTP traffic to HTTPS
server {
    listen 80;
    server_name qoestream.ait.ac.at;

	location ^~ /.well-known/acme-challenge/ {
        allow all;
        alias /var/www/.well-known/acme-challenge/;
        default_type "text/plain";
	}

   	return 301 https://$host$request_uri;
}

server {
    listen 443 ssl default deferred;
	server_name qoestream.ait.ac.at;
	
    ssl_certificate /etc/letsencrypt/live/qoestream.ait.ac.at/cert.pem;
	ssl_certificate_key /etc/letsencrypt/live/qoestream.ait.ac.at/privkey.pem;
	ssl_trusted_certificate /etc/letsencrypt/live/qoestream.ait.ac.at/chain.pem;

	# ssl_ciphers EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH;
    # ssl_protocols TLSv1.1 TLSv1.2;
	
	root /var/www;
    
    location /api {
    	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    	proxy_set_header Host $http_host;
    	proxy_set_header X-NginX-Proxy true;
    	proxy_http_version 1.1;
    	proxy_set_header Upgrade $http_upgrade;
    	proxy_set_header Connection "upgrade";
    	proxy_max_temp_file_size 0;
    	proxy_pass http://qoestream_v2_api;
    	proxy_redirect off;
    	proxy_read_timeout 240s;
    }

	location /grafana {
		proxy_pass http://qoestream_v2_grafana;
		rewrite ^/grafana/(.*) /$1 break;
		proxy_set_header Host $host;
    }

    location ^~ /.well-known/acme-challenge/ {
        allow all;
        alias /var/www/.well-known/acme-challenge/;
        default_type "text/plain";
	}
}
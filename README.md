# Installing instructions

1. Build and run docker containers by runing
```
docker-compose build
docker-compose up -d 
```

2. for production version run:
```
sudo docker-compose -f docker-compose.production.yml build --no-cache
sudo docker-compose -f docker-compose.production.yml up -d
```

3. To stop all services:
```
docker-compose stop
```

## Initialize DB with this command
```
docker-compose exec app ./node_modules/.bin/db-migrate up
```

## For testing DB run:
```
docker-compose exec app ./node_modules/.bin/db-migrate db:create qoestream_test
docker-compose exec app ./node_modules/.bin/db-migrate up --env test
```

## For running tests (first create qoestream_test db) 

```
docker-compose exec app yarn run test
```

# Restarting VM and Docker

To restart Ubuntu VM:

```bash
cd qoestream
sudo docker-compose stop
sudo reboot
```

After successfull reboot:

```bash
sudo docker-compose -f docker-compose.production.yml build --no-cache
sudo docker-compose -f docker-compose.production.yml up -d
```

# How to perform Let's encrypt renewal process:

Container should configured in a way, that renewal process requires only single command, which can be even put to CRON in the future:

```bash
sudo docker-compose -f docker-compose.production.yml run certbot renew
```

To see active server certificates, run following command: 

```bash
sudo docker-compose -f docker-compose.production.yml run certbot certificates
```

If any changes are done to nginx, container need to be rebuilt:

```bash
sudo docker-compose -f docker-compose.production.yml build web
```

## In case the previous commands fails, try these steps:


To issue new certificate use ```certbot``` docker image. Other docker containers must be also running (especially nginx, as the challenges folder must be accessible).

```bash
sudo docker-compose -f docker-compose.production.yml run certbot certonly --manual
```

Script will ask for which domain you want to issue new certificate: ```qoestream.ait.ac.at```

Manual process will ask you to put challange file with example content, so that it will be accessible at presented url:

e.g.: 

```bash
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Create a file containing just this data:

CQhIi6oY3jpG7K_AdTV1YUw9yDohIeddLGdYgJgKIts.HJpsyqpsLHMt0rlxO6zFynVx_YH8pdpxNBel727QxkI

And make it available on your web server at this URL:

http://qoestream.ait.ac.at/.well-known/acme-challenge/CQhIi6oY3jpG7K_AdTV1YUw9yDohIeddLGdYgJgKIts

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```

Do not confirm next step just yet, before that, create new file with given name (e.g. CQhIi6oY3jpG7K_AdTV1YUw9yDohIeddLGdYgJgKIts) and put it to ```.docker/challenges/``` folder. 
This will make it accessible at given url - which you can test be visiting it. 

If everything runs well, new certificates will be created in:

```bash
- Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/qoestream.ait.ac.at/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/qoestream.ait.ac.at/privkey.pem
   Your cert will expire on 2019-02-03. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot
   again. To non-interactively renew *all* of your certificates, run
   "certbot renew"
```

Nginx ("web" docker container) is set-up to have an access to these newly created files, and directly use them for HTTPS access. 
(In case nginx is unable to find them, modify access right to `./docker/certs/letsencrypt/etc/archive` and `./docker/certs/letsencrypt/etc/live`

If for whatever reason the path to certificates changes (it's not longer `/etc/letsencrypt/live/qoestream.ait.ac.at/fullchain.pem`), this can be changed directly 
in `.docker/vhosts-production.conf` file. 

If any changes are done to nginx, container need to be rebuilt:

```sudo docker-compose -f docker-compose.production.yml build web```

# Get Size of the DBs (Mysql and Mongo)

To get size of the mysql database, connect to mysql server via client (MySQL Workbench, Sequel Pro...) and execute this command:

```sql
SELECT table_schema AS "Database", 
ROUND(SUM(data_length + index_length) / 1024 / 1024 / 1024, 3) AS "Size (GB)" 
FROM information_schema.TABLES 
GROUP BY table_schema;
```

To get size of the MongoDB, you have to connect to server via SSH, navigate to qoestream folder and execute docker-compose command:


```sudo docker-compose -f docker-compose.production.yml exec mongo mongo admin -u admin -p pass```

From mongo shell switch to qoestream db and get it's size in GB:

```javascript
use qoestream
db.stats(1024*1024*1024)
```

# Install new grafana plugins from folder/zip

```bash
sudo docker cp grafana-json-datasource-cdn77 qoestream_grafana_1:/var/lib/grafana/plugins
```

SimpleJSON used in production is from this repo `https://github.com/simPod/grafana-json-datasource`, 
because the official one has problems with sending template variables to back-end as a part of the query. 
See https://github.com/grafana/simple-json-datasource/pull/100 for more info

# Docker containers and volumes backups

## How to copy data from docker volume to host machine folder (Manual copy from volume): 

Run following command to copy data from docker container volume to local data folder 

```bash
sudo docker cp $(sudo docker ps | grep grafana | awk '{print $1}'):/var/lib/grafana ./backups/grafana-data
```

### !IMPORTANT - before doing any of the following step, be sure that  all containers in docker-compose.*.yml file use the correct container_name
### Renaming is simply possible by changing the name directly in docker-compose file (container_name)

## Perform backups 
Tags: qs_prod_mysql, qs_prod_mongo, qs_prod_grafana are container names defined by container_name property in docker-compose.yml file.

It is possible to retrieve it automatically also by using bash variable e.g.: ```$(sudo docker ps | grep mysql | awk '{print $NF}')```

```bash
# MySQL
sudo docker run --rm --volumes-from qs_prod_v2_mysql -v $(pwd)/backups:/backups ubuntu tar cvf /backups/qs-mysql.tar /var/lib/mysql

# MongoDB
sudo docker run --rm --volumes-from qs_prod_v2_mongo -v $(pwd)/backups:/backups ubuntu tar cvf /backups/qs-mongo.tar /data/db

# Grafana
sudo docker run --rm --volumes-from qs_prod_v2_grafana -v $(pwd)/backups:/backups ubuntu tar cvf /backups/qs-grafana.tar /var/lib/grafana
```

## Copy backups to another server:

```bash
# MySQL
sudo scp vh795537@qoestream.ait.ac.at:/home/vh795537/qoestream/backups/qs-mysql.tar ~/qoestream/backups

# MongoDB
sudo scp vh795537@qoestream.ait.ac.at:/home/vh795537/qoestream/backups/qs-mongo.tar ~/qoestream/backups

# Grafana
sudo scp vh795537@qoestream.ait.ac.at:/home/vh795537/qoestream/backups/qs-grafana.tar ~/qoestream/backups
```

## Restore backups:
```bash
# MySQL
sudo docker run --rm --volumes-from qs_prod_v2_mysql -v $(pwd)/backups:/backups ubuntu bash -c "cd / && tar xvf /backups/qs-mysql.tar"
# MongoDB
sudo docker run --rm --volumes-from qs_prod_v2_mongo -v $(pwd)/backups:/backups ubuntu bash -c "cd / && tar xvf /backups/qs-mongo.tar"
# Grafana
sudo docker run --rm --volumes-from qs_prod_v2_grafana -v $(pwd)/backups:/backups ubuntu bash -c "cd / && tar xvf /backups/qs-grafana.tar"
```


# Rename docker volumes
## !!! DO THIS ONLY IF YOU ARE CHANGING DOCKER-COMPOSE VOLUME NAMES !!!

If new volumes are created manulay, it is necessary to change docker-compose file, to use external data volumes, with names. e.g.: 

```yml
volumes:
  qs_prod_grafana_data: 
    external: true
  qs_prod_sql_data: 
    external: true
  qs_prod_mongo_data: 
    external: true
```

For Grafana:
```bash
# Create new volume
sudo docker volume create --name qs_prod_grafana_data
# Get existing volume name (e.g. qoestream_qs2_gfdata)
sudo docker run --rm -it -v qoestream_qs2_gfdata:/from -v qs_prod_grafana_data:/to alpine ash -c "cd /from ; cp -av . /to"
# Check is the data was copied: 
sudo docker run --rm -it -v qs_prod_grafana_data:/data alpine ash -c "cd /data; ls -al"
# Remove old volume
sudo docker volume rm qoestream_qs2_gfdata
```

For MySQL:
```bash
sudo docker volume create --name qs_prod_sql_data

sudo docker run --rm -it -v qoestream_qs2_dbdata:/from -v qs_prod_sql_data:/to alpine ash -c "cd /from ; cp -av . /to"

sudo docker run --rm -it -v qs_prod_sql_data:/data alpine ash -c "cd /data; ls -al"

sudo docker volume rm qoestream_qs2_dbdata
```
For MongoDB:
```bash
sudo docker volume create --name qs_prod_mongo_data

sudo docker run --rm -it -v qoestream_qs2_mongo_data:/from -v qs_prod_mongo_data:/to alpine ash -c "cd /from ; cp -av . /to"

sudo docker run --rm -it -v qs_prod_mongo_data:/data alpine ash -c "cd /data; ls -al"

sudo docker volume rm qoestream_qs2_mongo_data
```

# Docker cleanup
Docker might take a lot of space by storing unused images, containers or volumes

To check current docker space usage, use this command: 

```bash
sudo docker system df
## For more detailed info
sudo docker system df -v
```

### Prune Images
To remove all images which are not used by existing containers, use the -a flag:
```bash
docker image prune -a
```

### Prune containers
When you stop a container, it is not automatically removed unless you started it with the --rm flag. To see all containers on the Docker host, including stopped containers, use docker ps -a. You may be surprised how many containers exist, especially on a development system! A stopped container's writable layers still take up disk space. To clean this up, you can use the docker container prune command.

```bash
docker container prune --filter "until=24h"
```

### Prune volumes
Volumes can be used by one or more containers, and take up space on the Docker host. Volumes are never removed automatically, because to do so could destroy data.

```bash
docker volume prune
```

### Prune everything
The docker system prune command is a shortcut that prunes images, containers, and networks. In Docker 17.06.0 and earlier, volumes are also pruned. In Docker 17.06.1 and higher, you must specify the --volumes flag for docker system prune to prune volumes.

```bash
docker system prune
## If you are on Docker 17.06.1 or higher and want to also prune volumes, add the --volumes flag:
docker system prune --volumes

WARNING! This will remove:
        - all stopped containers
        - all networks not used by at least one container
        - all volumes not used by at least one container
        - all dangling images
        - all build cache
Are you sure you want to continue? [y/N] y
```

Stop and remove all docker containers and images
1. List all containers (only IDs) ```docker ps -aq```.
2. Stop all running containers. ```docker stop $(docker ps -aq)```
3. Remove all containers. ```docker rm $(docker ps -aq)```
4. Remove all images. ```docker rmi $(docker images -q)```

# CRON Jobs

Most of the crons jobs are defined as nodejs jobs and are located within withing ```./crons/*.js``` files and sre controlled by central ```./crons/index.js``` file, where each cronjob can be enabled and also its execution time can be changed there. These jobs are run by PM2 manager, and the runner process is configured in ```ecossytem.config.js``` in the root folder.

Sometimes there is a necessity to run CRON jobs outside of docker containers - mostly if it is not related to nodeJs API. For this reason there are additional system defined cron scripts located in ```./crons/system-crons/*.sh```. If you add any new script to this folder, you have to also create executable rights e.g.:

```bash
sudo chmod +x ./crons/system-crons/backup-dbs.sh
sudo chmod +x ./crons/system-crons/backup-grafana.sh
```

All system CRONS are set by command (don't forget sudo - as the cron should run as a root job):

```bash
sudo crontab -e
```

This command opens file with CRON settings:

```bash
0 1 * * * /home/vh795537/qoestream/crons/system-crons/backup-dbs.sh
30 1 * * * /home/vh795537/qoestream/crons/system-crons/backup-grafana.sh
```

## Current CRON schedule:

At 2:00 AM - stop docker images, perform database backups (mysql, mongo) to /backups forlder and start docker containers again

At 2:30 AM - Perform grafana backup to /backups folder

In PM2: 

At 3:00 AM - Perform prune of old session records

At 3:30 AM - Precalculate episode-metrics
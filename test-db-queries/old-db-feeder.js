const request = require("request-promise");

// @DONE
// const min_session_idx = 3050;
// const max_session_idx = 3084;

// @DONE
// const min_session_idx = 3084;
// const max_session_idx = 3100;

// @DONE
// const min_session_idx = 3100;
// const max_session_idx = 3120;

const min_session_idx = 3178;
const max_session_idx = 3180;

var sessionCountIterator = {
    *[Symbol.iterator]() {
        for (let i = min_session_idx; i < max_session_idx; i++) {
            yield i;
        }
        // // @TODO: Temporary add more sessions
        // for (let k = 3120; k < 3170; k++) {
        //     yield k;
        // }
    }
}

feedResults().then(res => {
    console.log(res);
});

async function feedResults() {
    let start = Date.now();
    for (let i of sessionCountIterator) {
        // let res = await fakeAsync(i);
        let res = await get( i*1000 );
        console.log(i * 1000);
        console.log(res);
        console.log((Date.now() - start) / 1000,'s');
    }

    return 'Done';
}

/**
 * Get user by ID
 * 
 * @param {*} req 
 * @param {*} res 
 */
async function get(skip = 0) {
    try {
        return await request.get(
            `http://0.0.0.0:3001/api/v2/feeds/${skip}`, 
            {
                rejectUnauthorized: false
            }
        );
    } catch (err) {
        console.log(err);
    }
}

function fakeAsync(i) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(i);
        }, 1000);
    });
}
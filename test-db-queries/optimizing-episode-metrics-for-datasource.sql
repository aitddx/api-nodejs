EXPLAIN SELECT COUNT(*) 
FROM (
	SELECT 
	UNIX_TIMESTAMP(timestamp) DIV 21600 * 21600 as time, 
	initial_delay as value, 
	sess.isp_id, 
	sess.operating_system_id, 
	sess.platform_id, 
	sess.interface_type_id 
FROM episode_metrics 
INNER JOIN sessions as sess 
ON episode_metrics.`session_id` = sess.`id` 
WHERE timestamp BETWEEN '2019-01-20 12:30:00.000' AND '2019-01-27 12:30:00.000' AND live = 1 AND initial_delay < 100000 AND initial_delay > 0 AND stallings_count <= 15
) as sub;

EXPLAIN SELECT 
	UNIX_TIMESTAMP(timestamp) DIV 21600 * 21600 as time, 
	initial_delay as value, 
	sess.isp_id, 
	sess.operating_system_id, 
	sess.platform_id, 
	sess.interface_type_id 
FROM episode_metrics 
INNER JOIN sessions as sess 
ON episode_metrics.`session_id` = sess.`id` 
WHERE timestamp BETWEEN '2019-01-20 12:30:00.000' AND '2019-01-27 12:30:00.000' AND live = 1 AND initial_delay < 100000 AND initial_delay > 0 AND stallings_count <= 15;

EXPLAIN SELECT s.isp_id, i.`name` as `name` FROM sessions as s JOIN isps as i ON s.isp_id = i.id WHERE s.session_start BETWEEN '2019-01-20 12:30:00.000' AND '2019-01-27 12:30:00.000' GROUP BY s.isp_id ORDER BY count(s.isp_id) DESC LIMIT 10;

-- DROP INDEX episode_timestamp_initial_delay_session ON episode_metrics;
DROP INDEX episode_timestamp_live_aborted_idx ON episode_metrics;

ALTER TABLE episode_metrics ADD outlier BOOLEAN AFTER stallings_average;
-- ALTER TABLE episode_metrics DROP COLUMN outlier;

UPDATE episode_metrics
SET outlier = IF((initial_delay < 100000 AND initial_delay >= 0 AND stallings_count <= 15) IS TRUE, FALSE, TRUE);

CREATE INDEX episode_outliers_init_delay
ON episode_metrics (`outlier`, `live`, `initial_delay`, `session_id`, `timestamp`);
-- DROP INDEX episode_outliers_init_delay ON episode_metrics;

-- EXPLAIN EXTENDED
SELECT 
	UNIX_TIMESTAMP(timestamp) DIV 21600 * 21600 as time, 
	initial_delay as value, 
	aborted_episode,
	episode_metrics.platform_id,
	sess.isp_id, 
	sess.operating_system_id, 
	sess.platform_id, 
	sess.interface_type_id 
FROM episode_metrics 
INNER JOIN sessions as sess 
ON episode_metrics.`session_id` = sess.`id` 
WHERE outlier = 0 AND live = 1 AND timestamp BETWEEN '2019-01-20 12:30:00.000' AND '2019-02-20 12:30:00.000' AND episode_metrics.platform_id = 2;

-- EXPLAIN EXTENDED
SELECT 
	UNIX_TIMESTAMP(timestamp) DIV 21600 * 21600 as time, 
	initial_delay as value, 
	sess.isp_id, 
	sess.operating_system_id, 
	sess.platform_id, 
	sess.interface_type_id 
FROM episode_metrics 
INNER JOIN sessions as sess 
ON episode_metrics.`session_id` = sess.`id` 
WHERE outlier = 0 AND live = 1 AND timestamp BETWEEN '2019-01-20 12:30:00.000' AND '2019-02-20 12:30:00.000';

select count(*) from (
SELECT 
	UNIX_TIMESTAMP(timestamp) DIV 21600 * 21600 as time, 
	initial_delay as value, 
	sess.isp_id, 
	sess.operating_system_id, 
	sess.platform_id, 
	sess.interface_type_id 
FROM episode_metrics 
INNER JOIN sessions as sess 
ON episode_metrics.`session_id` = sess.`id` 
WHERE outlier = 0 AND live = 1 AND timestamp BETWEEN '2019-01-20 12:30:00.000' AND '2019-02-20 12:30:00.000'
) as sub;
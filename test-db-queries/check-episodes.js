require('dotenv').config();

const fs = require('fs');
const mongo = require('../api/services/mongo');

let mongoDb;
const min_limit = 1528977600000;
const max_limit = 1529012700000;

init();

/**
 * Run script
 */
async function init() {
    mongoDb = await mongo.connect();
    const episodesFile = 'test-db-queries/episodes_wc_14_6_2018.json';
    let episodes = [];

    try {
        episodes = await readFile(episodesFile);

        if (episodes.length == 0) {
            let res = await getEpisodesWithinLimits(min_limit, max_limit);
            await res.forEach(x => episodes.push(...x.episodes));
            episodes = episodes.filter(x => 
                x.episode_end > min_limit && x.episode_end < max_limit
            );
            // Save to disk to speed up processing if 
            // the results have been already retrieved from DB
            await writeFile(episodesFile, episodes);
        }
        
        analyzeEpisodes(episodes);
        analyzeEpisodesFilteredByStallingDuration(episodes, 50)

    } catch (error) {
        console.log(error);
    }

    await mongoDb.close();
}

/**
 * Analyze episodes array and print not started episodes ratio
 * 
 * @param {array} episodes Array to analyze
 */
function analyzeEpisodes(episodes) {
    liveEpisodes = episodes.filter(x => x.episode_length === 0);
    vodEpisodes = episodes.filter(x => x.episode_length !== 0);

    const notStartedAll = episodes.reduce(countNotStartedEpisodes, 0);
    const notStartedLive = liveEpisodes.reduce(countNotStartedEpisodes, 0);
    const notStartedVod = vodEpisodes.reduce(countNotStartedEpisodes, 0);

    console.log('Not started All: ', Math.round(notStartedAll / episodes.length * 1000)/10,'%');
    console.log('Not started Live: ', Math.round(notStartedLive / liveEpisodes.length * 1000) / 10, '%');
    console.log('Not started VoD: ', Math.round(notStartedVod / vodEpisodes.length * 1000) / 10, '%');
    console.log('live ' + liveEpisodes.length);
    console.log('VoD ' + vodEpisodes.length);
}

/**
 * analyze only episodes where stalling was shorter then limit
 * 
 * @param {int} stallingDuration stallings filter
 */
function analyzeEpisodesFilteredByStallingDuration(episodes, stallingDuration) {
    episodes = episodes.map( episode => {
        return { 
            ...episode,
            stallings: episode.stallings.map(stall => stall.stalling_end - stall.stalling_start)
        };
    });
    let filtered = episodes.filter(episode => 
        !episode.stallings.reduce( (prev, curr) => 
            curr > stallingDuration * 1000 || prev, false
        )
    );

    console.log('Unfiltered: ', episodes.length);
    console.log('Filtered: ', filtered.length);
    // console.log(filtered.slice(0,100).map( _ => _.stallings));
    analyzeEpisodes(filtered);
}

/**
 * Reducer function for counting not started episodes
 * 
 * @param {int} prev Previous value for reducer
 * @param {int} curr Current value for reducer
 */
function countNotStartedEpisodes(prev, curr) {
    const notStarted = curr.stallings.length == 0 ? 1 : 0;
    return prev + notStarted;
}

/**
 * 
 * @param {*} user_id User_id from mysql DB
 * @param {*} session 
 */
async function getEpisodesWithinLimits(min, max) {
    let mongoSessions = await mongoDb
        .db('qoestream')
        .collection('sessions');

    let mongoResult = await mongoSessions.find({
        "episodes.episode_end": {
            $gt: min,
            $lt: max
        }
    }, {
        "episodes": 1,
        _id: 0
    });

    return mongoResult;
}

/**
 * Create promise from readFile reader
 * 
 * @param {string} path File name
 */
async function readFile(path) {
    return new Promise( (resolve, reject) => {
        fs.readFile(path, 'utf8', (err, data) => {
            if (err && err.errno !== -2) {
                return reject(err);
            };
            if (err && err.errno === -2) {
                return resolve([]);
            };
            return resolve(JSON.parse(data));
        })
    });
}

/**
 * Create promise from readFile reader
 * 
 * @param {string} path File name
 */
async function writeFile(path, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, JSON.stringify(data), 'utf8', (err, data) => {
            if (err) return reject(err);
            resolve(data);
        })
    });
}
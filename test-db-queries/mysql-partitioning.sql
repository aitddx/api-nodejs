ALTER TABLE sessions PARTITION BY RANGE ( TO_DAYS(session_start) ) (
    PARTITION fromstart VALUES LESS THAN (TO_DAYS('2018-11-25')),
    PARTITION from201847 VALUES LESS THAN (TO_DAYS('2018-12-02')),
    PARTITION from201848 VALUES LESS THAN (TO_DAYS('2018-12-09')),
    PARTITION from201849 VALUES LESS THAN (TO_DAYS('2018-12-16')),
    PARTITION from201850 VALUES LESS THAN (TO_DAYS('2018-12-23')),
    PARTITION from201851 VALUES LESS THAN (TO_DAYS('2018-12-30')),
    PARTITION from201852 VALUES LESS THAN (TO_DAYS('2019-01-06')),
    PARTITION from201901 VALUES LESS THAN (TO_DAYS('2019-01-13')),
    PARTITION from201902 VALUES LESS THAN (TO_DAYS('2019-01-20')),
    PARTITION from201903 VALUES LESS THAN (TO_DAYS('2019-01-27')),
    PARTITION from201904 VALUES LESS THAN (TO_DAYS('2019-01-03')),
    PARTITION from201905 VALUES LESS THAN (TO_DAYS('2019-02-10')),
    PARTITION from201906 VALUES LESS THAN (TO_DAYS('2019-02-17')),
    PARTITION future VALUES LESS THAN MAXVALUE
);

ALTER TABLE episode_metrics PARTITION BY RANGE ( TO_DAYS(TIMESTAMP) ) (
    PARTITION fromstart VALUES LESS THAN (TO_DAYS('2018-11-25')),
    PARTITION from201847 VALUES LESS THAN (TO_DAYS('2018-12-02')),
    PARTITION from201848 VALUES LESS THAN (TO_DAYS('2018-12-09')),
    PARTITION from201849 VALUES LESS THAN (TO_DAYS('2018-12-16')),
    PARTITION from201850 VALUES LESS THAN (TO_DAYS('2018-12-23')),
    PARTITION from201851 VALUES LESS THAN (TO_DAYS('2018-12-30')),
    PARTITION from201852 VALUES LESS THAN (TO_DAYS('2019-01-06')),
    PARTITION from201901 VALUES LESS THAN (TO_DAYS('2019-01-13')),
    PARTITION from201902 VALUES LESS THAN (TO_DAYS('2019-01-20')),
    PARTITION from201903 VALUES LESS THAN (TO_DAYS('2019-01-27')),
    PARTITION from201904 VALUES LESS THAN (TO_DAYS('2019-01-03')),
    PARTITION from201905 VALUES LESS THAN (TO_DAYS('2019-02-10')),
    PARTITION from201906 VALUES LESS THAN (TO_DAYS('2019-02-17')),
    PARTITION future VALUES LESS THAN MAXVALUE
);

actions -> session_id
episode_metrics -> session_id
episodes -> session_id
bitrates -> episode_id
stallings -> episode_id
sessions -> session_start


CREATE TABLE `episode_metrics` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `session_id` INT(10) UNSIGNED NOT NULL,
  `episode_id` INT(10) UNSIGNED NOT NULL,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `platform_id` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
  `adaptive_streaming` TINYINT(1) DEFAULT NULL,
  `live` TINYINT(1) DEFAULT NULL,
  `aborted_episode` TINYINT(1) DEFAULT NULL,
  `initial_delay` INT(11) DEFAULT NULL,
  `stallings_count` TINYINT(3) UNSIGNED DEFAULT NULL,
  `stallings_average` DECIMAL(7,2) DEFAULT NULL,
  `timestamp` DATETIME(3) NULL DEFAULT NULL,
  PRIMARY KEY (`id`,`timestamp`),
  KEY `episode_metrics_timestamp_index` (`timestamp`),
  KEY `episode_timestamp_live_idx` (`timestamp`,`live`),
  KEY `episode_timestamp_live_aborted_idx` (`timestamp`,`live`,`aborted_episode`),
  KEY `episode_timestamp_initial_delay_session` (`timestamp`,`session_id`,`initial_delay`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO qoestream_partitions.episode_metrics SELECT * FROM qoestream.episode_metrics;
ALTER TABLE episode_metrics DROP PRIMARY KEY, ADD PRIMARY KEY(id, timestamp);
###See: https://stackoverflow.com/questions/36791662/mysql-monthwise-partitioning-scanning-all-partitions-instead-of-one
ALTER TABLE episode_metrics PARTITION BY RANGE ( TO_DAYS(TIMESTAMP) ) (
    PARTITION fromstart VALUES LESS THAN (TO_DAYS('2018-11-11')),
    PARTITION from201845 VALUES LESS THAN (TO_DAYS('2018-11-18')),
    PARTITION from201846 VALUES LESS THAN (TO_DAYS('2018-11-25')),
    PARTITION from201847 VALUES LESS THAN (TO_DAYS('2018-12-02')),
    PARTITION from201848 VALUES LESS THAN (TO_DAYS('2018-12-09')),
    PARTITION from201849 VALUES LESS THAN (TO_DAYS('2018-12-16')),
    PARTITION from201850 VALUES LESS THAN (TO_DAYS('2018-12-23')),
    PARTITION from201851 VALUES LESS THAN (TO_DAYS('2018-12-30')),
    PARTITION from201852 VALUES LESS THAN (TO_DAYS('2019-01-06')),
    PARTITION from201901 VALUES LESS THAN (TO_DAYS('2019-01-13')),
    PARTITION from201902 VALUES LESS THAN (TO_DAYS('2019-01-20')),
    PARTITION from201903 VALUES LESS THAN (TO_DAYS('2019-01-27')),
    PARTITION from201904 VALUES LESS THAN (TO_DAYS('2019-02-03')),
    PARTITION from201905 VALUES LESS THAN (TO_DAYS('2019-02-10')),
    PARTITION from201906 VALUES LESS THAN (TO_DAYS('2019-02-17')),
    PARTITION future VALUES LESS THAN MAXVALUE
);

CREATE TABLE `isps` (
  `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO qoestream_partitions.isps SELECT * FROM qoestream.isps;

INSERT INTO qoestream_partitions.sessions SELECT * FROM qoestream.sessions ORDER BY session_start DESC LIMIT 3000000;
INSERT INTO qoestream_partitions.sessions_partitioned SELECT * FROM qoestream.sessions ORDER BY session_start DESC LIMIT 3000000;

ALTER TABLE sessions_partitioned MODIFY session_start DATETIME(3);
ALTER TABLE sessions_partitioned DROP PRIMARY KEY, ADD PRIMARY KEY(id, session_start);
ALTER TABLE sessions_partitioned PARTITION BY RANGE ( TO_DAYS(session_start) ) (
    PARTITION fromstart VALUES LESS THAN (TO_DAYS('2018-11-25')),
    PARTITION from201847 VALUES LESS THAN (TO_DAYS('2018-12-02')),
    PARTITION from201848 VALUES LESS THAN (TO_DAYS('2018-12-09')),
    PARTITION from201849 VALUES LESS THAN (TO_DAYS('2018-12-16')),
    PARTITION from201850 VALUES LESS THAN (TO_DAYS('2018-12-23')),
    PARTITION from201851 VALUES LESS THAN (TO_DAYS('2018-12-30')),
    PARTITION from201852 VALUES LESS THAN (TO_DAYS('2019-01-06')),
    PARTITION from201901 VALUES LESS THAN (TO_DAYS('2019-01-13')),
    PARTITION from201902 VALUES LESS THAN (TO_DAYS('2019-01-20')),
    PARTITION from201903 VALUES LESS THAN (TO_DAYS('2019-01-27')),
    PARTITION from201904 VALUES LESS THAN (TO_DAYS('2019-02-03')),
    PARTITION from201905 VALUES LESS THAN (TO_DAYS('2019-02-10')),
    PARTITION from201906 VALUES LESS THAN (TO_DAYS('2019-02-17')),
    PARTITION future VALUES LESS THAN MAXVALUE
);



SHOW ENGINE INNODB STATUS;

SELECT * FROM sessions WHERE platform_id = 1 LIMIT 10;

SELECT * FROM sessions WHERE session_start BETWEEN '2019-01-01 04:45:26.777' AND '2019-01-02 04:45:26.777';
SELECT * FROM sessions_partitioned WHERE session_start BETWEEN '2019-01-01 04:45:26.777' AND '2019-01-02 04:45:26.777';

SELECT id, session_start, TO_DAYS(session_start), YEARWEEK(session_start) AS week_day FROM sessions WHERE YEARWEEK(session_start) = 201906;

SELECT TO_DAYS('2017-05-15');




EXPLAIN PARTITIONS SELECT COUNT(1) FROM ( SELECT UNIX_TIMESTAMP(TIMESTAMP) DIV 1800 * 1800 AS TIME, initial_delay AS VALUE, sess.isp_id, sess.operating_system_id, sess.platform_id, sess.interface_type_id FROM episode_metrics INNER JOIN sessions AS sess ON episode_metrics.`session_id` = sess.`id` WHERE TIMESTAMP BETWEEN '2019-02-04T17:27:51.017Z' AND '2019-02-09T17:27:51.017Z' AND live = 1 AND initial_delay < 100000 AND initial_delay > 0 AND stallings_count <= 15 ) t;
SELECT COUNT(1) FROM ( SELECT UNIX_TIMESTAMP(TIMESTAMP) DIV 1800 * 1800 AS TIME, initial_delay AS VALUE, sess.isp_id, sess.operating_system_id, sess.platform_id, sess.interface_type_id FROM episode_metrics INNER JOIN sessions AS sess ON episode_metrics.`session_id` = sess.`id` WHERE TIMESTAMP BETWEEN '2019-02-04T17:27:51.017Z' AND '2019-02-09T17:27:51.017Z' AND live = 1 AND initial_delay < 100000 AND initial_delay > 0 AND stallings_count <= 15 ) t;
SELECT COUNT(1) FROM ( SELECT UNIX_TIMESTAMP(TIMESTAMP) DIV 1800 * 1800 AS TIME, initial_delay AS VALUE, sess.isp_id, sess.operating_system_id, sess.platform_id, sess.interface_type_id FROM episode_metrics INNER JOIN sessions AS sess ON episode_metrics.`session_id` = sess.`id` WHERE TIMESTAMP BETWEEN '2019-01-20 17:27:51.017' AND '2019-02-09 17:27:51.017' AND live = 1 AND initial_delay < 100000 AND initial_delay > 0 AND stallings_count <= 15 ) t;




EXPLAIN PARTITIONS SELECT COUNT(1) FROM ( SELECT UNIX_TIMESTAMP(TIMESTAMP) DIV 1800 * 1800 AS TIME, initial_delay AS VALUE, sess.isp_id, sess.operating_system_id, sess.platform_id, sess.interface_type_id FROM episode_metrics INNER JOIN sessions_partitioned AS sess ON episode_metrics.`session_id` = sess.`id` WHERE TIMESTAMP BETWEEN '2019-02-04 17:27:51.017' AND '2019-02-09 17:27:51.017' AND live = 1 AND initial_delay < 100000 AND initial_delay > 0 AND stallings_count <= 15 ) t;
SELECT COUNT(1) FROM ( SELECT UNIX_TIMESTAMP(TIMESTAMP) DIV 1800 * 1800 AS TIME, initial_delay AS VALUE, sess.isp_id, sess.operating_system_id, sess.platform_id, sess.interface_type_id FROM episode_metrics INNER JOIN sessions_partitioned AS sess ON episode_metrics.`session_id` = sess.`id` WHERE TIMESTAMP BETWEEN '2019-02-04 17:27:51.017' AND '2019-02-09 17:27:51.017' AND live = 1 AND initial_delay < 100000 AND initial_delay > 0 AND stallings_count <= 15 ) t;


EXPLAIN PARTITIONS SELECT UNIX_TIMESTAMP(TIMESTAMP) DIV 1800 * 1800 AS TIME FROM episode_metrics WHERE TIMESTAMP BETWEEN '2019-02-04T17:27:51.017Z' AND '2019-02-09T17:27:51.017Z';
SELECT COUNT(1) FROM (SELECT UNIX_TIMESTAMP(TIMESTAMP) DIV 1800 * 1800 AS TIME FROM episode_metrics WHERE TIMESTAMP BETWEEN '2019-02-04T17:27:51.017Z' AND '2019-02-09T17:27:51.017Z' AND live = 1 AND initial_delay < 100000 AND initial_delay > 0 AND stallings_count <= 15) t;

SELECT 
     table_schema as `Database`, 
     table_name AS `Table`, 
     round(((data_length + index_length) / 1024 / 1024), 1) `Size in MB` 
FROM information_schema.TABLES 
WHERE table_name LIKE "%epis%"
ORDER BY (data_length + index_length) DESC;


SELECT COUNT(1) FROM (SELECT id from sessions WHERE session_start < DATE_SUB(NOW(), INTERVAL 90 DAY)) t;

SELECT count(1) FROM (SELECT id from episode_metrics WHERE `session_id` IN (SELECT * FROM (SELECT id from sessions WHERE session_end < DATE_SUB(NOW(), INTERVAL 90 DAY)) as sub) ) t;

SELECT count(1) FROM (
SELECT em.id from episode_metrics em
LEFT JOIN sessions s ON em.session_id = s.id
WHERE session_end < DATE_SUB(NOW(), INTERVAL 90 DAY)
) t;

SELECT count(1) FROM (SELECT em.* FROM episode_metrics em
WHERE NOT EXISTS ( SELECT 1 FROM sessions WHERE sessions.id = em.session_id ) ) t;

SELECT count(1) FROM (
SELECT em.id from episode_metrics em
LEFT JOIN sessions s ON em.session_id = s.id
WHERE s.id IS NULL
) t;

SELECT em.id from episode_metrics em
LEFT JOIN sessions s ON em.session_id = s.id
WHERE session_end < DATE_SUB(NOW(), INTERVAL 90 DAY);

DELETE em.* from episode_metrics em
LEFT JOIN sessions s ON em.session_id = s.id
WHERE session_end < DATE_SUB(NOW(), INTERVAL 90 DAY);

DELETE e.* from episodes e
LEFT JOIN sessions s ON e.session_id = s.id
WHERE session_end < DATE_SUB(NOW(), INTERVAL 90 DAY);

DELETE a.* from actions a
LEFT JOIN sessions s ON a.session_id = s.id
WHERE session_end < DATE_SUB(NOW(), INTERVAL 90 DAY);


### Proper way how to delete main record and delete orphaned data
### Main tables
DELETE s.* FROM sessions s WHERE session_end < DATE_SUB(NOW(), INTERVAL 90 DAY);

SELECT count(1) FROM (
	SELECT em.id from episode_metrics em
	LEFT JOIN sessions s ON em.session_id = s.id
	WHERE s.id IS NULL
) t;

EXPLAIN DELETE em.* from episode_metrics em
LEFT JOIN sessions s ON em.session_id = s.id
WHERE s.id IS NULL;

OPTIMIZE TABLE episode_metrics;

SELECT count(1) FROM (
	SELECT e.id from episodes e
	LEFT JOIN sessions s ON e.session_id = s.id
	WHERE s.id IS NULL
) t;

DELETE e.* from episodes e
LEFT JOIN sessions s ON e.session_id = s.id
WHERE s.id IS NULL;

OPTIMIZE TABLE episodes;

SELECT count(1) FROM (
	SELECT a.id from actions a
	LEFT JOIN sessions s ON a.session_id = s.id
	WHERE s.id IS NULL
) t;

DELETE a.* from actions a
LEFT JOIN sessions s ON a.session_id = s.id
WHERE s.id IS NULL;

OPTIMIZE TABLE actions;

SELECT count(1) FROM (
	SELECT b.id from bitrates b
	LEFT JOIN episodes e ON b.episode_id = e.id
	WHERE e.id IS NULL
) t;

DELETE b.* from bitrates b
LEFT JOIN episodes e ON b.episode_id = e.id
WHERE e.id IS NULL;

OPTIMIZE TABLE bitrates;

SELECT count(1) FROM (
	SELECT s.id from stallings s
	LEFT JOIN episodes e ON s.episode_id = e.id
	WHERE e.id IS NULL
) t;

DELETE s.* from stallings s
LEFT JOIN episodes e ON s.episode_id = e.id
WHERE e.id IS NULL;

OPTIMIZE TABLE stallings;

SELECT count(1) FROM (
SELECT em.id from episode_metrics em
LEFT JOIN sessions s ON em.session_id = s.id
WHERE session_end < DATE_SUB(NOW(), INTERVAL 90 DAY)
) t;
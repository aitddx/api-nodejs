'use strict';
require('dotenv').config();
const app = require('./app');

// Constants
let PORT = process.env.APP_PORT || 3000;
let HOST = process.env.APP_HOST || '0.0.0.0';

// Run server
app.listen(PORT, () => {
    console.log(`Running on http://${HOST}:${PORT}`);
});

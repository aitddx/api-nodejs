//Require the dev-dependencies
require('dotenv').config();
const chai = require('chai');
const chaiHttp = require('chai-http');
const fs = require("fs");
const present = require('present');
require('it-each')();

const app = require('../app');
const db = require('../api/models');
const jwt = require('jsonwebtoken');

const users = require('../api/models/users');

chai.use(chaiHttp);
chai.should();

var jwtToken;
var numberOfSessions2Test = 20;

describe('Sessions', () => {
    
    beforeEach(async () => {
        await cleanup_db();
        jwtToken = await createJwtToken();
    });

    after(async () => { 
        await cleanup_db();
    });

    /*
     * Test the /POST route
     */
    describe('/POST V1 sessions', () => {
        var sessions = JSON.parse(fs.readFileSync('test-jsons.json'));
        var t0;
        var acc = 0;
        var count = 0;

        it.each(sessions.slice(0, numberOfSessions2Test), 'it should POST one session to API', (element, next) => {
            t0 = present();
            chai.request(app)
                .post('/api/v1/session')
                .set('content-type', 'application/x-www-form-urlencoded')
                .set('authorization', 'Bearer ' + jwtToken)
                .send({data: { session: element}})
                .end( (err, res) => {
                    res.status.should.be.within(200, 201);
                    res.body.should.be.a('Object');
                    
                    acc += (present() - t0);
                    count += 1;
                    console.log(`avg: ${acc / count}, count: ${count}`);
                    
                    next();
                });
        });

        it('it should record urlencoded session POST', async () => {
            let res = await chai.request(app)
                .post('/api/v1/session')
                .set('content-type', 'application/x-www-form-urlencoded')
                .set('authorization', 'Bearer ' + jwtToken)
                .send({data: getFakeSessionString()});

            res.status.should.be.within(200, 201);
            res.body.should.be.a('Object');
        });
    });

    /*
     * Test the /POST route
     */
    describe('/POST V2 sessions', () => {
        var sessions = JSON.parse(fs.readFileSync('test-jsons.json'));
        var t0;
        var acc = 0;
        var count = 0;

        it.each(sessions.slice(0, numberOfSessions2Test), 'it should POST one session to API', (element, next) => {
            t0 = present();
            chai.request(app)
                .post('/api/v2/sessions')
                .set('content-type', 'application/x-www-form-urlencoded')
                .set('authorization', 'Bearer ' + jwtToken)
                .send({data: {session: element}})
                .end((err, res) => {
                    res.status.should.be.within(200, 201);
                    res.body.should.be.a('Object');

                    acc += (present() - t0);
                    count += 1;
                    console.log(`avg: ${acc / count}, count: ${count}`);

                    next();
                });
        });

        it('it should record urlencoded session POST', async () => {
            let res = await chai.request(app)
                .post('/api/v2/sessions')
                .set('content-type', 'application/x-www-form-urlencoded')
                .set('authorization', 'Bearer ' + jwtToken)
                .send({
                    data: getFakeSessionString()
                })

            res.status.should.be.within(200, 201);
            res.body.should.be.a('Object');
        });

        it('it should fail to record urlencoded session POST if auth header is wrong', async () => {
            let res = await chai.request(app)
                .post('/api/v2/sessions')
                .set('content-type', 'application/x-www-form-urlencoded')
                .set('authorization', 'Bearer InvalidToken')
                .send({
                    data: getFakeSessionString()
                })

            res.status.should.equal(401);
        });

        describe('Platform headers:', () => {
            it('it should record session if iOS header is sent', async () => {
                return testPlatformStrings('iOS', 1);
            });

            it('it should record session if iOS header is sent in lowercase', async () => {
                return testPlatformStrings('ios', 1);
            });

            it('it should record session if Android header is sent', async () => {
                return testPlatformStrings('Android', 2);
            });

            it('it should record session if Web header is sent', async () => {
                return testPlatformStrings('Web', 3);
            });

            it('it should record session if incorrect header is sent', async () => {
                return testPlatformStrings('not known platform', 0);
            });

            it('it should record session if platform header is not set', async () => {
                return testPlatformStrings('not known platform', 0);
            });
        });
    });
});

async function testPlatformStrings(platformString = '', platformCode = 0) {
    let request = chai.request(app)
        .post('/api/v2/sessions')
        .set('content-type', 'application/x-www-form-urlencoded')
        .set('authorization', 'Bearer ' + jwtToken);

    if (platformString != '') {
        request.set('x-qs-platform', platformString);
    }

    let res = await request.send({
        data: getFakeSessionString()
    });

    const activeConnection = await db.connect();
    let row = await db.query('SELECT * FROM `sessions`');
    await activeConnection.release();

    chai.expect(row.slice(-1).pop().platform_id).to.equal(platformCode);
    res.status.should.equal(201);
}

async function cleanup_db() {
    let conn = Object.create(db);
    const activeConnection = await conn.connect();
    await conn.query(`
        TRUNCATE TABLE client_versions;
        TRUNCATE TABLE device_types;
        TRUNCATE TABLE isps;
        TRUNCATE TABLE operating_systems;
        TRUNCATE TABLE episode_contents;
        TRUNCATE TABLE episodes;
        TRUNCATE TABLE bitrates;
        TRUNCATE TABLE actions;
        TRUNCATE TABLE action_types;
        TRUNCATE TABLE stallings;
        TRUNCATE TABLE episode_metrics;
        TRUNCATE TABLE sessions;
        TRUNCATE TABLE users;
    `);
    await activeConnection.release();
}

async function createTestingUser() {
    try {
        const activeConnection = await db.connect();
        let userId = await users.findOrCreate({
            uuid: 'tester'
        });
        await activeConnection.release();

        return userId;
    } catch(err) {
        console.log(err);
    }
    
}

/**
 * Create temporary JWT token
 * 
 * @param {number} userId user to generate token for
 */
async function createJwtToken() {
    try {
        let userId = await createTestingUser();

        return jwt.sign(
            { userId },
            process.env.APP_SECRET, 
            { expiresIn: "1h" }
        );
    } catch (err) {
        console.log(err);
    }
}

function getFakeSessionString() {
    return "%7B%22session%22%3A%7B%22client_version%22%3A%223.6.3%22%2C%22interface%22%3A%22WiFi%22%2C%22session_start%22%3A%221528990331208%22%2C%22episodes%22%3A%5B%7B%22episode_end%22%3A%221528990369943%22%2C%22episode_start%22%3A%221528990347637%22%2C%22episode_id%22%3A%22preroll_13979624%22%2C%22episode_length%22%3A3047%2C%22episode_title%22%3A%22Willkommen%20%C3%96sterreich%20mit%20Matthias%20Strolz%20%26%20Hubert%20Kramar%22%2C%22segment_id%22%3Anull%2C%22bitrates%22%3A%5B%7B%22bitrate%22%3A1057054%2C%22timestamp%22%3A%221528990354748%22%7D%5D%2C%22stallings%22%3A%5B%7B%22stalling_start%22%3A%221528990349990%22%2C%22stalling_end%22%3A%221528990354676%22%7D%5D%7D%2C%7B%22episode_end%22%3A%221528990372940%22%2C%22episode_start%22%3A%221528990369943%22%2C%22episode_id%22%3A%2213979624%22%2C%22episode_length%22%3A3047%2C%22episode_title%22%3A%22Willkommen%20%C3%96sterreich%20mit%20Matthias%20Strolz%20%26%20Hubert%20Kramar%22%2C%22segment_id%22%3Anull%2C%22bitrates%22%3A%5B%7B%22bitrate%22%3A992000%2C%22timestamp%22%3A%221528990370754%22%7D%2C%7B%22bitrate%22%3A3192000%2C%22timestamp%22%3A%221528990372273%22%7D%5D%2C%22stallings%22%3A%5B%7B%22stalling_start%22%3A%221528990369943%22%2C%22stalling_end%22%3A%221528990370515%22%7D%5D%7D%5D%2C%22os_version%22%3A%2211.4%22%2C%22session_end%22%3A%221528990379807%22%2C%22device_type%22%3A%22Simulator%22%2C%22actions%22%3A%5B%7B%22action%22%3A%22videoplayer_started%22%2C%22timestamp%22%3A%221528990349319%22%7D%2C%7B%22action%22%3A%22videoplayer_ended%22%2C%22timestamp%22%3A%221528990372409%22%7D%2C%7B%22action%22%3A%22app_focus_lost%22%2C%22timestamp%22%3A%221528990379807%22%7D%5D%2C%22ISP%22%3A%22UPC%20Austria%22%7D%7D";
}
'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = async function (db, callback) {
  try {
    await db.runSql(
      'ALTER TABLE sessions ADD column platform_id SMALLINT(5) unsigned DEFAULT NULL AFTER client_version_id'
    );
    await db.runSql(
      'ALTER TABLE episode_metrics ADD column platform_id SMALLINT(5) unsigned DEFAULT NULL AFTER user_id'
    );
    await db.runSql(
      'ALTER TABLE client_versions ADD column platform_id SMALLINT(5) unsigned DEFAULT NULL AFTER version'
    );
    await db.runSql(
      'ALTER TABLE device_types ADD column platform_id SMALLINT(5) unsigned DEFAULT NULL AFTER type'
    );
    await db.runSql(
      'ALTER TABLE operating_systems ADD column platform_id SMALLINT(5) unsigned DEFAULT NULL AFTER name'
    );
  } catch (err) {
    callback(err);
  }
};

exports.down = async function (db, callback) {
  try {
    await db.runSql('ALTER TABLE sessions DROP COLUMN platform_id');
    await db.runSql('ALTER TABLE episode_metrics DROP COLUMN platform_id');
    await db.runSql('ALTER TABLE client_versions DROP COLUMN platform_id');
    await db.runSql('ALTER TABLE device_types DROP COLUMN platform_id');
    await db.runSql('ALTER TABLE operating_systems DROP COLUMN platform_id');
  } catch (err) {
    callback(err);
  }
};

exports._meta = {
  "version": 1
};
'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  return db.createTable('stallings', {
    id: {
      type: 'int',
      unsigned: true,
      primaryKey: true,
      length: 10,
      autoIncrement: true
    },
    episode_id: {
      type: 'int',
      length: 10,
      unsigned: true,
      notNull: true
    },
    order: {
      type: 'smallint',
      unsigned: true
    },
    stalling_start: {
      type: 'timestamp',
      length: 3,
      notNull: false
    },
    duration: {
      type: 'int',
      length: 10,
      notNull: true
    }
  }, createIndices);

  function createIndices(err) {
    if (err) {
      callback(err);
      return;
    }

    db.addIndex('bitrates', 'bitrates_episode_id_index', ['episode_id'], (err) => callback(err));
  }

};

exports.down = function (db) {
  return db.dropTable('stallings');
};

exports._meta = {
  "version": 1
};

'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  db.createTable('episodes', {
    id: {
      type: 'int',
      unsigned: true,
      primaryKey: true,
      autoIncrement: true,
      length: 10
    },
    session_id: {
      type: 'int',
      length: 10,
      unsigned: true,
      notNull: true
    },
    order: {
      type: 'tinyint',
      length: 3,
      unsigned: true
    },
    episode_detail_id: {
      type: 'int',
      length: 10,
      unsigned: true,
      notNull: true
    },
    segment_id: {
      type: 'int',
      notNull: false,
      length: 10,
      unsigned: true
    },
    episode_length: {
      type: 'int',
      length: 10,
      unsigned: true
    },
    episode_start: {
      type: 'timestamp',
      length: 3,
      notNull: false
    },
    episode_end: {
      type: 'timestamp',
      length: 3,
      notNull: false
    }
  }, createIndices);

  function createIndices(err) {
    if (err) {
      callback(err);
      return;
    }

    db.addIndex('episodes', 'episodes_session_id_index', ['session_id'], (err) => callback(err))
    db.addIndex('episodes', 'episodes_episode_detail_id_index', ['episode_detail_id'], (err) => callback(err))
    db.addIndex('episodes', 'episodes_episode_start_index', ['episode_start'], (err) => callback(err))
  }
};

exports.down = function(db) {
  return db.dropTable('episodes');
};

exports._meta = {
  "version": 1
};

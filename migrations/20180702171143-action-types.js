'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  return db.createTable('action_types', {
    id: {
      type: 'tinyint',
      unsigned: true,
      primaryKey: true,
      length: 3,
      autoIncrement: true
    },
    type: {
      type: 'string',
      length: 30,
      notNull: true,
      notNull: true
    }
  });

};

exports.down = function (db) {
  return db.dropTable('action_types');
};

exports._meta = {
  "version": 1
};

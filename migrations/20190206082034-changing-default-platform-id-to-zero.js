'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = async function (db, callback) {
  try {
    await db.runSql(
      'TRUNCATE TABLE `platforms`'
    );
    await db.runSql(
      'ALTER TABLE `platforms` MODIFY `id` SMALLINT(5) UNSIGNED NOT NULL'
    );
    await db.runSql(
      'INSERT INTO platforms (id,name) VALUES(0, \'unknown\'), (1, \'iOS\'), (2, \'Android\'), (3, \'Web\')'
    );
    await db.runSql(
      'ALTER TABLE sessions MODIFY COLUMN platform_id SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0'
    );
    await db.runSql(
      'ALTER TABLE episode_metrics MODIFY COLUMN platform_id SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0'
    );
    await db.runSql(
      'ALTER TABLE client_versions MODIFY COLUMN platform_id SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0'
    );
    await db.runSql(
      'ALTER TABLE device_types MODIFY COLUMN platform_id SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0'
    );
    await db.runSql(
      'ALTER TABLE operating_systems MODIFY COLUMN platform_id SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0'
    );
  } catch (err) {
    callback(err);
  }
};

exports.down = async function (db, callback) {
    try {
      await db.runSql(
        'TRUNCATE TABLE `platforms`'
      );
      await db.runSql(
        'ALTER TABLE `platforms` MODIFY `id` SMALLINT(5) UNSIGNED NOT NULL auto_increment'
      );
      await db.runSql(
        'INSERT INTO platforms (id,name) VALUES (1, \'iOS\'), (2, \'Android\'), (3, \'Web\')'
      );
      await db.runSql(
        'ALTER TABLE sessions MODIFY COLUMN platform_id SMALLINT(5) unsigned DEFAULT NULL'
      );
      await db.runSql(
        'ALTER TABLE episode_metrics MODIFY COLUMN platform_id SMALLINT(5) unsigned DEFAULT NULL'
      );
      await db.runSql(
        'ALTER TABLE client_versions MODIFY COLUMN platform_id SMALLINT(5) unsigned DEFAULT NULL'
      );
      await db.runSql(
        'ALTER TABLE device_types MODIFY COLUMN platform_id SMALLINT(5) unsigned DEFAULT NULL'
      );
      await db.runSql(
        'ALTER TABLE operating_systems MODIFY COLUMN platform_id SMALLINT(5) unsigned DEFAULT NULL'
      );
    } catch (err) {
      callback(err);
    }
};

exports._meta = {
  "version": 1
};
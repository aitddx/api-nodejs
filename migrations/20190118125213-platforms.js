'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
   return db.createTable('platforms', {
     id: {
       type: 'smallint',
       notNull: true,
       unsigned: true,
       primaryKey: true,
       length: 5,
       autoIncrement: true
     },
     name: {
       type: 'string',
       notNull: true,
       length: 30
     }
   }, addPlatforms);

   function addPlatforms(err) {
     console.log('Creating default platforms...');
     if (err) {
       callback(err);
       return;
     }

     db.connection.query("INSERT INTO platforms (id,name) VALUES(1, 'iOS'), (2, 'Android'), (3, 'Web')", callback);
   }
};

exports.down = function(db) {
  return db.dropTable('platforms');
};

exports._meta = {
  "version": 1
};

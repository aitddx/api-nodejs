'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  return db.createTable('interface_types', {
    id: {
      type: 'tinyint',
      notNull: true,
      unsigned: true,
      primaryKey: true,
      length: 3,
      autoIncrement: true
    },
    type: {
      type: 'string',
      notNull: true,
      length: 30
    }
  });

};

exports.down = function (db) {
  return db.dropTable('interface_types');
};

exports._meta = {
  "version": 1
};

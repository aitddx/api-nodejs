mongo << EOF
use qoestream
db.sessions.createIndex(
    { user_id: 1, session_start: 1 }
)
EOF
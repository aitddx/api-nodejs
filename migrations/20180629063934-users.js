'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.createTable('users', {
    id: {
      type: 'int',
      notNull: true,
      unsigned: true,
      primaryKey: true,
      autoIncrement: true,
      length: 10
    },
    uuid: {
      type: 'char',
      notNull: true,
      length: 36
    },
    tester: {
      type: 'boolean',
      notNull: true,
      defaultValue: false
    }
  }, createTimestamps);

  function createTimestamps(err) {
    if (err) { callback(err); return; }

    db.connection.query(`
    ALTER TABLE users
    ADD created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
      ON UPDATE CURRENT_TIMESTAMP
    `, (err) => callback(err) );
  }

};

exports.down = function(db) {
  return db.dropTable('users');
};

exports._meta = {
  "version": 1
};

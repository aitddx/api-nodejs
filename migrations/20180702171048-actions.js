'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  return db.createTable('actions', {
    id: {
      type: 'int',
      unsigned: true,
      primaryKey: true,
      length: 10,
      autoIncrement: true
    },
    session_id: {
      type: 'int',
      length: 10,
      unsigned: true, 
      notNull: true
    },
    action_type_id: {
      type: 'tinyint',
      unsigned: true,
      notNull: true
    },
    timestamp: {
      type: 'timestamp',
      length: 3,
      notNull: false
    }
  }, createIndices);

  function createIndices(err) {
    if (err) {
      callback(err);
      return;
    }

    db.addIndex('actions', 'actions_session_id_index', ['session_id'], (err) => callback(err));
  }

};

exports.down = function (db) {
  return db.dropTable('actions');
};

exports._meta = {
  "version": 1
};

'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  return db.createTable('episode_contents', {
    id: {
      type: 'int',
      unsigned: true,
      primaryKey: true,
      length: 10,
      autoIncrement: true
    },
    episode_content_id: {
      type: 'string',
      length: 20,
      notNull: true
    },
    episode_title: {
      type: 'string',
      length: 255,
      notNull: true
    }
  }, createIndices);

  function createIndices(err) {
    if (err) {
      callback(err);
      return;
    }

    db.addIndex('episode_contents', 'episode_contents_episode_content_id_index', ['episode_content_id'], (err) => callback(err));
    db.addIndex('episode_contents', 'episode_contents_episode_title_index', ['episode_title'], (err) => callback(err));
  }

};

exports.down = function (db) {
  return db.dropTable('episode_contents');
};

exports._meta = {
  "version": 1
};

/* Replace with your SQL commands */

CREATE INDEX `episode_timestamp_live_aborted_idx` ON episode_metrics (`timestamp`,`live`,`aborted_episode`);
CREATE INDEX `episode_timestamp_initial_delay_session` ON episode_metrics (`timestamp`,`session_id`,`initial_delay`)

DROP INDEX episode_outliers_init_delay ON episode_metrics;
ALTER TABLE episode_metrics DROP COLUMN outlier;
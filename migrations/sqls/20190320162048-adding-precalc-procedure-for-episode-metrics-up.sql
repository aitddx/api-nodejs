/* Replace with your SQL commands */
DROP PROCEDURE IF EXISTS updateLiveOverview;

CREATE DEFINER=`root`@`%` PROCEDURE `updateLiveOverview`(IN BINSTART INT, IN BINEND INT, IN BINSIZE INT)
BEGIN
INSERT INTO ts_overview 
       (SELECT a.bin, a.v_initd_avg, a.v_stall_cnt_avg, a.v_stall_dur_avg, a.v_stall_null_cnt, b.v_nse_cnt, a.v_se_cnt, (b.v_nse_cnt+a.v_se_cnt) AS ep_cnt, a.d_live, a.d_platform
       FROM (SELECT from_unixtime(cast(cast(UNIX_TIMESTAMP(episode_metrics.timestamp)/(BINSIZE) as signed)*BINSIZE as signed)) AS bin, 
              AVG(initial_delay) AS v_initd_avg, 
              AVG(stallings_count) AS v_stall_cnt_avg,
              AVG(stallings_average) AS v_stall_dur_avg,
              SUM(CASE WHEN stallings_average IS NULL THEN 1 ELSE 0 END) AS v_stall_null_cnt,
              COUNT(*) as v_se_cnt,
              live as d_live,
              platform_id as d_platform 
              FROM qoestream.episode_metrics 
              WHERE initial_delay < 100000 AND initial_delay > 0 AND stallings_count <= 15 AND timestamp >= now()  - INTERVAL BINSTART DAY AND timestamp < now() - INTERVAL BINEND DAY
              GROUP BY bin, d_live, d_platform 
              ORDER BY timestamp ASC) AS a
        JOIN (SELECT from_unixtime(cast(cast(UNIX_TIMESTAMP(episode_metrics.timestamp)/(BINSIZE) as signed)*BINSIZE as signed)) AS bin, 
              SUM(aborted_episode) AS v_nse_cnt,
              live as d_live,
              platform_id as d_platform
              FROM qoestream.episode_metrics 
              WHERE timestamp >= now() - INTERVAL BINSTART DAY AND timestamp < now() - INTERVAL BINEND DAY 
              GROUP BY bin, d_live, d_platform
              ORDER BY timestamp ASC) as b
         ON a.bin=b.bin AND a.d_live=b.d_live AND a.d_platform=b.d_platform)
ON DUPLICATE KEY UPDATE
      bin=VALUES(bin),
      v_initd_avg=VALUES(v_initd_avg),
      v_stall_cnt_avg=VALUES(v_stall_cnt_avg),
      v_stall_dur_avg=VALUES(v_stall_dur_avg),
      v_stall_null_cnt=VALUES(v_stall_null_cnt),
      v_nse_cnt=VALUES(v_nse_cnt),
      v_se_cnt=VALUES(v_se_cnt),
      ep_cnt=VALUES(ep_cnt),
      d_live=VALUES(d_live),
      d_platform=VALUES(d_platform);
END
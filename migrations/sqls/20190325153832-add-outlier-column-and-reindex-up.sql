/* Replace with your SQL commands */

DROP INDEX episode_timestamp_initial_delay_session ON episode_metrics;
DROP INDEX episode_timestamp_live_aborted_idx ON episode_metrics;

ALTER TABLE episode_metrics ADD outlier BOOLEAN AFTER stallings_average;

UPDATE episode_metrics
SET outlier = IF(
    (initial_delay < 100000 AND initial_delay >= 0 AND stallings_count <= 15) IS TRUE, 
    FALSE, 
    TRUE
);

CREATE INDEX episode_outliers_init_delay
ON episode_metrics (`outlier`, `live`, `initial_delay`, `session_id`, `timestamp`);
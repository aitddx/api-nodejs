'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  return db.createTable('client_versions', {
    id: {
      type: 'smallint',
      notNull: true,
      unsigned: true,
      primaryKey: true,
      length: 5,
      autoIncrement: true
    },
    version: {
      type: 'char',
      notNull: true,
      length: 8
    }
  });

};

exports.down = function (db) {
  return db.dropTable('client_versions');
};

exports._meta = {
  "version": 1
};

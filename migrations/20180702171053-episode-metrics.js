'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  return db.createTable('episode_metrics', {
    id: {
      type: 'int',
      unsigned: true,
      primaryKey: true,
      length: 10,
      autoIncrement: true
    },
    session_id: {
      type: 'int',
      length: 10,
      unsigned: true,
      notNull: true
    },
    episode_id: {
      type: 'int',
      length: 10,
      unsigned: true,
      notNull: true
    },
    user_id: {
      type: 'int',
      length: 10,
      unsigned: true,
      notNull: true
    },
    adaptive_streaming: {
      type: 'boolean',
      notNull: false,
    },
    live: {
      type: 'boolean',
      notNull: false
    },
    aborted_episode: {
      type: 'boolean',
      notNull: false
    },
    initial_delay: {
      type: 'int',
      notNull: false
    },
    stallings_count: {
      type: 'tinyint',
      unsigned: true,
      notNull: false
    },
    stallings_average: {
      type: 'decimal',
      length: '7,2',
      notNull: false
    },
    timestamp: {
      type: 'timestamp',
      length: 3,
      notNull: false
    }
  }, createIndices);

  function createIndices(err) {
    if (err) {
      callback(err);
      return;
    }

    db.addIndex('episode_metrics', 'episode_metrics_session_id_index', ['session_id'], (err) => callback(err));
    db.addIndex('episode_metrics', 'episode_metrics_episode_id_index', ['episode_id'], (err) => callback(err));
    db.addIndex('episode_metrics', 'episode_metrics_user_id_index', ['user_id'], (err) => callback(err));
    db.addIndex('episode_metrics', 'episode_metrics_live_index', ['live'], (err) => callback(err));
    db.addIndex('episode_metrics', 'episode_metrics_timestamp_index', ['timestamp'], (err) => callback(err));
  }

};

exports.down = function (db) {
  return db.dropTable('episode_metrics');
};

exports._meta = {
  "version": 1
};

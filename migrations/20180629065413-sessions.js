'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db, callback) {
  return db.createTable('sessions', {
    id: {
      type: 'int',
      notNull: true,
      unsigned: true,
      primaryKey: true,
      autoIncrement: true,
      length: 10
    },
    user_id: {
      type: 'int',
      notNull: true,
      length: 10,
      unsigned: true
    },
    isp_id: {
      type: 'smallint',
      notNull: false,
      length: 5,
      unsigned: true
    },
    device_type_id: {
      type: 'smallint',
      notNull: true,
      length: 5,
      unsigned: true
    },
    operating_system_id: {
      type: 'smallint',
      notNull: true,
      length: 5,
      unsigned: true
    },
    interface_type_id: {
      type: 'tinyint',
      notNull: true,
      length: 3,
      unsigned: true
    },
    client_version_id: {
      type: 'smallint',
      notNull: true,
      length: 5,
      unsigned: true
    },
    session_start: {
      type: 'timestamp',
      length: 3,
      notNull: false
    },
    session_end: {
      type: 'timestamp',
      length: 3,
      notNull: false
    }
  }, createTimestamps);

  function createTimestamps(err) {
    if (err) {
      callback(err);
      return;
    }

    db.connection.query(`
    ALTER TABLE sessions
    ADD created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
      ON UPDATE CURRENT_TIMESTAMP
    `, createIndices);
  }

  function createIndices(err) {
    if (err) {
      callback(err);
      return;
    }

    db.addIndex('sessions', 'sessions_user_id_index', ['user_id'], (err) => callback(err))
    db.addIndex('sessions', 'sessions_isp_id_index', ['isp_id'], (err) => callback(err))
    db.addIndex('sessions', 'sessions_device_type_id_index', ['device_type_id'], (err) => callback(err))
    db.addIndex('sessions', 'sessions_session_start_index', ['session_start'], (err) => callback(err))
  }
};

exports.down = function(db) {
  return db.dropTable('sessions');
};

exports._meta = {
  "version": 1
};

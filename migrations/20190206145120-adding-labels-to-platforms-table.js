'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = async function(db, callback) {
  try {
    await db.dropTable('platforms');
    await db.createTable('platforms', {
      id: {
        type: 'smallint',
        notNull: true,
        unsigned: true,
        primaryKey: true,
        length: 5,
        autoIncrement: false
      },
      name: {
        type: 'string',
        notNull: true,
        length: 30
      },
      label: {
        type: 'string',
        notNull: true,
        length: 30
      }
    });
    await db.runSql("INSERT INTO platforms (id, name, label) VALUES (0, 'unknown', 'Unknown'), (1, 'ios', 'iOS'), (2, 'android', 'Android'), (3, 'web', 'Web')", callback);
  } catch (err) {
    callback(err);
  }
};

exports.down = function(db) {
  return null;
};

exports._meta = {
  "version": 1
};
